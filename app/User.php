<?php

namespace Multiweaver;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'timezone', 'looking_for', 'birthdate', 'steam_id', 'play_time_from', 'play_time_to', 'about', 'maturity'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

	function games() {
		return $this->belongsToMany('\Multiweaver\Game')->withPivot('platform_id', 'platform_name');
	}

	public function suggestedUsers()
	{
		return $this->belongsToMany('\Multiweaver\User', 'suggested_users', 'user_id', 'suggested_user_id');
	}

	public function suggestedFor()
	{
		return $this->belongsToMany('\Multiweaver\User', 'suggested_users', 'suggested_user_id', 'user_id');
	}

	function conversations() {
		return $this->belongsToMany('\Multiweaver\Conversation');
	}

	function seenMessages() {
		return $this->belongsToMany("\Multiweaver\Message", 'user_saw_message');
	}

	function languages() {
		return $this->belongsToMany('\Multiweaver\Language');
	}

	function communication_methods() {
		return $this->belongsToMany('\Multiweaver\CommunicationMethod');
	}

	function commonGames(User $user) {
		$gameIds = $user->games()->lists('id');
		return $this->games()->whereIn('games.id', $gameIds);
	}

	function suggestions() {
		$users = User::select('users.*')->join('game_user', 'users.id', '=', 'game_user.user_id')->groupBy('users.id');

		$users->where('users.id', '<>', $this->id);

		$gameIds = $this->games()->lists('id');

		$users = $users->join('games', 'game_user.game_id', '=', 'games.id');
		$users = $users->whereIn('games.id', $gameIds);

		$users = $users->where('users.looking_for', $this->looking_for);

		$timezone_offset = $this->timezone_offset;

		$gmt_time_from = intval($this->gmt_time_from);
		$gmt_time_to = intval($this->gmt_time_to);

		$users = $users->whereRaw('(('.$gmt_time_to.' < '.$gmt_time_from.' AND ('.$gmt_time_to.' >= gmt_time_from OR '.$gmt_time_from.' < gmt_time_to)) OR (gmt_time_from < gmt_time_to AND ('.$gmt_time_to.' > gmt_time_from AND '.$gmt_time_from.' < gmt_time_to)) OR (gmt_time_from >= gmt_time_to AND ('.$gmt_time_to.' < gmt_time_to OR '.$gmt_time_from.' >= gmt_time_from)))');

		$users = $users->join('communication_method_user', 'communication_method_user.user_id', '=', 'users.id');
		$methodIds = $this->communication_methods()->lists('id');
		$users = $users->whereIn('communication_method_user.communication_method_id', $methodIds);

		$users = $users->join('language_user', 'language_user.user_id', '=', 'users.id');
		$languageIds = $this->languages()->lists('id');
		$users = $users->whereIn('language_user.language_id', $languageIds);

		if ($this->maturity == 1) {
			$users->where('users.birthdate', '<', date('Y-m-d', time()-86400*365*21));
		}

		return $users;
	}

	function languageString() {
		$languages = [];
		foreach($this->languages()->get() as $lang) {

			$languages[] = $lang->name ? $lang->name : $lang->english_name;
		}
		if (count($languages) == 0) $languages[] = 'no languages';
		return implode(', ', $languages);
	}

	function communicationString() {
		$methods = [];
		foreach($this->communication_methods()->get() as $method) {

			$methods[] = $method->name;
		}
		if (count($methods) == 0) $methods[] = 'nothing';
		return implode(', ', $methods);
	}

	function isInterested(Game $game) {
		return $this->games()->get()->contains($game->id);
	}

	function getPlatform(Game $game) {
		$game = $this->games->find($game->id);
		$name = $game->pivot->platform_name ? "(".$game->pivot->platform_name.")" : ($game->platforms->count() > 1 ? '(Any Platform)' : '');
		return $name;
	}
}
