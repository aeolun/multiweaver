<?php

namespace Multiweaver;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	function user() {
		return $this->belongsTo("\Multiweaver\User");
	}

	function conversation() {
		return $this->belongsTo("\Multiweaver\Conversation");
	}

	function seenby() {
		return $this->belongsToMany("\Multiweaver\User", 'user_saw_message');
	}
}
