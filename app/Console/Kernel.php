<?php

namespace Multiweaver\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Multiweaver\Console\Commands\Inspire::class,
        \Multiweaver\Console\Commands\UpdateGames::class,
        \Multiweaver\Console\Commands\MailSuggestions::class,
        \Multiweaver\Console\Commands\MailMissedMessages::class,
        \Multiweaver\Console\Commands\UpdateUsers::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        $schedule->call(function () {
            $result = DB::update('update games inner join (select game_id, count(*) nr from game_user group by game_id) t1 ON games.id = t1.game_id SET games.popularity = t1.nr');
            echo "updates $result games\n";
        })->everyFiveMinutes();
        $schedule->command('suggestions:send')->weekly()->fridays()->at('00:00');
        $schedule->command('missed:send')->hourly();
        $schedule->call(function() {
            $result = DB::update('update users set random_order = RAND()');
        })->daily();
    }
}
