<?php

namespace Multiweaver\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Multiweaver\EmailLogin;
use Multiweaver\Game;
use Multiweaver\User;

class MailSuggestions extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'suggestions:send {start?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends all users suggestions for people to play with';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$start = $userId = $this->argument('start');

		$users = User::get();

		$bar = $this->output->createProgressBar(count($users));
		$bar->setMessage("Starting...");
		$bar->setFormat("%message%\n  %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");

		$i = 0;
		foreach($users as $user) {
			$timezoneName = timezone_name_from_abbr("", $user->timezone_offset*3600, false);
			$user->timezone = $timezoneName;
			/***********************************
			 * DO NOT SEND TO LIVE USERS!
			 **********************************/
			if (env('APP_ENV', 'local') == 'local' && $user->id != 2) continue;

			$bar->setMessage($user->name);
			$bar->advance();
			$i++;
			if ($start && $i < $start) continue;

			$alreadySuggested = $user->suggestedUsers()->lists('id');
			$suggestions = $user->suggestions()->whereNotIn('users.id', $alreadySuggested)->get()->all();
			//don't send suggestion email if we don't have any
			if (count($suggestions) == 0) continue;
			shuffle($suggestions);
			$suggestions = array_slice($suggestions, 0, 4);

			$updateGamesLink = new EmailLogin($user->id, Config::get('app.url').'users/'.$user->id);
			$updateGamesLink->save();

			foreach($suggestions as $idx => $suggestion) {
				$contactLink = new EmailLogin($user->id, Config::get('app.url').'conversations?users='.$suggestion->id.'#new');
				$contactLink->save();

				$suggestions[$idx]->contactLink = $contactLink;
			}

			Mail::send('emails.suggestions', array('user' => $user, 'suggestions'=>$suggestions, 'gameUpdateLink'=>$updateGamesLink), function($message) use ($user, $suggestions)
			{
				$message->to($user->email, $user->name)->subject(count($suggestions).' new player suggestions from Multiweaver');
			});

			foreach($suggestions as $suggestedUser) {
				$user->suggestedUsers()->attach($suggestedUser->id);
			}
		}
		$bar->finish();
	}
}
