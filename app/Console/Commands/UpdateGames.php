<?php

namespace Multiweaver\Console\Commands;

use Carbon\Carbon;
use Guzzle\Http\Client;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Multiweaver\Category;
use Multiweaver\Company;
use Multiweaver\Game;
use Multiweaver\Genre;
use Multiweaver\Language;
use Multiweaver\Platform;
use Multiweaver\Screenshot;

class UpdateGames extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'games:update {--full}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates the games in the database';

    protected $newNames = [];

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle($fullUpdate = false)
	{
	    $names = Game::all()->pluck('name')->all();

        $this->scroll('/games/', [ 'fields' => '*' ], [$this, 'scrollHandler'], true);

        $oldMatches = array_intersect($names, $this->newNames);
        $this->output->writeln(count($oldMatches).' out of '.count($names).' match name');
	}

	private function scrollHandler($responseBody) {
	    $data = json_decode($responseBody, true);

        foreach($data as $game) {
            $this->newNames[] = $game['name'];
        }

        $this->output->writeln("Retrieved " .count($data). "games");
    }

	private function get($endpoint = '', $parameters = []) {
        $hash = $endpoint.md5(json_encode($parameters)).'.json';
        $cacheFile = 'storage/app/cache/'.$hash;
        $cacheDir = dir($cacheFile);

        if ( ! is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }

        if (file_exists($cacheFile)) {
            $data = file_get_contents($cacheFile);
        } else {
            $client = new Client('https://api-2445582011268.apicast.io/');

            $request = $client->get($endpoint);
            $request->addHeader('user-key', '6c4006c8bc7d62b6f626f49d0c222cbc');
            $request->addHeader('Accept', 'application/json');

            $request->getParams()->replace($parameters);

            $response = $request->send();

            $data = $response->getBody(true);
            file_put_contents($cacheFile, $data);
        }

        $json = json_decode($data, true);
        return $json;
    }

    private function scroll($endpoint = '', $parameters = [], callable $handler, $cached = true) {
        $start = 0;
        $perPage = 50;
        $total = 100;

        $cacheDir = 'storage/app/cache/';

        if ( ! is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }

        $client = new Client('https://api-2445582011268.apicast.io');

        if ($cached) {
            $hash = trim($endpoint, '/').md5(json_encode($parameters)).'.scroll.'.$start.'.json';
            $cacheFile = $cacheDir.$hash;

            while(file_exists($cacheFile)) {
                $this->output->writeln("start $start");

                $data = file_get_contents($cacheFile);

                $handler($data);

                $start += $perPage;
                $hash = trim($endpoint, '/').md5(json_encode($parameters)).'.scroll.'.$start.'.json';
                $cacheFile = $cacheDir.$hash;
            }
        } else {
            $this->output->writeln("GET: https://api-2445582011268.apicast.io$endpoint");
            $this->output->writeln("start $start");

            $request = $client->createRequest('GET', $endpoint);
            $request->addHeader('user-key', '6c4006c8bc7d62b6f626f49d0c222cbc');
            $request->addHeader('Accept', 'application/json');

            $params = $request->getQuery();
            $params->replace($parameters);
            $params->add('scroll', 1);
            $params->add('limit', $perPage);

            $response = $request->send();

            $total = $response->getHeader('x-count')->toArray()[0];
            $next = $response->getHeader('x-next-page')->toArray()[0];

            $this->output->writeln(json_encode($response->getHeaders()));

            $hash = trim($endpoint, '/').md5(json_encode($parameters)).'.scroll.'.$start.'.json';
            $cacheFile = $cacheDir.$hash;

            file_put_contents($cacheFile, $response->getBody(true));

            $start += $perPage;

            $handler($response->getBody(true));

            while($start < $total) {
                $this->output->writeln("GET: $next");
                $this->output->writeln("start $start / $total");
                $request->setUrl('https://api-2445582011268.apicast.io'.$next);

                $params = $request->getQuery();
                $params->replace($parameters);

                $response = $request->send();
                $this->output->writeln("Response ".$response->getStatusCode());

                $hash = trim($endpoint, '/').md5(json_encode($parameters)).'.scroll.'.$start.'.json';
                $cacheFile = $cacheDir.$hash;

                file_put_contents($cacheFile, $response->getBody(true));

                $start += $perPage;

                $handler($response->getBody(true));
            }
        }
    }
}
