<?php

namespace Multiweaver\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Multiweaver\Conversation;
use Multiweaver\EmailLogin;
use Multiweaver\Game;
use Multiweaver\Message;
use Multiweaver\User;

class MailMissedMessages extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'missed:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends all users the messages they have missed in the last hour';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$conversations = Conversation::select('conversations.*')->join('messages', function($query) {
			$query->on('messages.conversation_id', '=', 'conversations.id')->where('messages.created_at', '>', date('Y-m-d H:i:s', time()-3600))->where('messages.created_at', '<', date('Y-m-d H:i:s'));
		})->groupBy('conversations.id')->get();

		foreach($conversations as $conversation) {
			$this->getOutput()->writeln($conversation->title);
			foreach($conversation->users as $user) {
				$missedMessages = $conversation->unseenMessagesFor($user)->where('messages.created_at', '>', date('Y-m-d H:i:s', time()-3600))->where('messages.created_at', '<', date('Y-m-d H:i:s'))->get();
				if($missedMessages->count() > 0) {
					/***********************************
					 * DO NOT SEND TO LIVE USERS!
					 **********************************/
					if (env('APP_ENV', 'local') == 'local' && $user->id != 2) continue;

					$timezoneName = timezone_name_from_abbr("", $user->timezone_offset*3600, false);

					$this->getOutput()->writeln("Sending ".$missedMessages->count().' to '.$user->name.' for convo '.$conversation->title. ' in '.$timezoneName);
					$missedMessageLink = new EmailLogin($user->id, Config::get('app.url').'conversations#'.$conversation->id);
					$missedMessageLink->save();

					Mail::send('emails.missed', array('conversation' => $conversation, 'emailLogin'=> $missedMessageLink, 'missed' => $missedMessages, 'user' => $user, 'timezone'=>$timezoneName), function ($message) use ($user, $conversation, $missedMessages) {
						$message->to($user->email, $user->name)->subject(count($missedMessages).' new messages in ' . $conversation->title.' on Multiweaver');
					});
				}
			}
		}
	}
}
