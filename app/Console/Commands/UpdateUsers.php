<?php

namespace Multiweaver\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Multiweaver\Conversation;
use Multiweaver\EmailLogin;
use Multiweaver\Game;
use Multiweaver\Message;
use Multiweaver\User;
use DateTimeZone;
use Carbon\Carbon;

class UpdateUsers extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'users:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates all users';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$users = User::all();
		foreach($users as $user) {
			echo "{$user->email}\n";

			$user->gmt_time_from = (new Carbon($user->play_time_from.':00:00', new DateTimeZone($user->timezone)))->setTimeZone('GMT')->format('H');
			$user->gmt_time_to = (new Carbon($user->play_time_to.':00:00', new DateTimeZone($user->timezone)))->setTimeZone('GMT')->format('H');
			if ($user->gmt_time_to == 0) $user->gmt_time_to = 24;

			$user->save();
		}
	}
}
