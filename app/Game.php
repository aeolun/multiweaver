<?php

namespace Multiweaver;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Game extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'logo_url', 'multiplayer', 'type'];

	function users() {
		return $this->belongsToMany('\Multiweaver\User');
	}

	function platforms() {
		return $this->belongsToMany('\Multiweaver\Platform');
	}

	function categories() {
		return $this->belongsToMany('\Multiweaver\Category');
	}

	function genres() {
		return $this->belongsToMany('\Multiweaver\Genre');
	}

	function languages() {
		return $this->belongsToMany('\Multiweaver\Language');
	}

	function publishers() {
		return $this->belongsToMany('\Multiweaver\Company', 'game_publisher');
	}

	function developers() {
		return $this->belongsToMany('\Multiweaver\Company', 'game_developer');
	}

	function screenshots() {
		return $this->hasMany('\Multiweaver\Screenshot');
	}
}
