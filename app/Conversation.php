<?php

namespace Multiweaver;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	function users() {
		return $this->belongsToMany('\Multiweaver\User');
	}

	function unseenMessagesFor(User $user) {
		return Message::select('messages.*')->leftJoin('user_saw_message', function ($join) use ($user) {
			$join->on('messages.id', '=', 'user_saw_message.message_id')
				->where('user_saw_message.user_id', '=', $user->id);
		})->where('messages.conversation_id', $this->id)->whereNull('user_saw_message.user_id');
	}

	function messages() {
		return $this->hasMany("\Multiweaver\Message");
	}
}
