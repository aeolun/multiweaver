<?php

namespace Multiweaver;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class EmailLogin extends Model
{
	function __construct($user_id=null, $redirect_url=null) {
		parent::__construct();
		$this->unique_id = $this->v4();
		$this->user_id = $user_id;
		$this->redirect = $redirect_url;
		$this->expires = Carbon::now()->addDays(7);
	}

	public static function v4()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),
			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,
			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

    function getUrl() {
	    return Config::get('app.url').'login/'.$this->unique_id;
    }
}
