<?php

namespace Multiweaver\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Multiweaver\Conversation;
use Multiweaver\Http\Requests;
use Multiweaver\Http\Controllers\Controller;
use Multiweaver\Message;
use Multiweaver\User;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->exists('users')) {
            $ids = explode(',', $request->get('users'));
            $users = User::whereIn('id', $ids);
            $conv = new Conversation();
            $conv->title = implode(', ',$users->lists('name')->toArray());
            $conv->save();
            $conv->users()->attach(Auth::user()->id);
            foreach(User::whereIn('id', $ids)->get() as $user) {
                $conv->users()->attach($user->id);
            }
            return redirect(url('conversations#'.$conv->id));
        }

        return view('conversation/index', [
            'conversations' => Auth::user()->conversations()->orderBy('updated_at', 'desc')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $conv = new Conversation();
        $conv->title = $request->get('title');
        $conv->save();
        $conv->users()->attach(Auth::user()->id);
        foreach($request->get('conversation_users') as $user_id) {
            $conv->users()->attach($user_id);
        }
    }

    public function add(Request $request, $conversationId, $userId) {
        $conversation = Conversation::find($conversationId);
        $conversation->users()->detach($userId);
        $conversation->users()->attach($userId);
        $conversation->touch();
        $conversation->save();

        $request->session()->flash('status', 'Added user!');
        return back();
    }

    public function message(Request $request, $id) {
        $conversation = Conversation::find($request->get('conversation_id'));

        if (!$conversation->users->contains(Auth::user())) return;

        $conversation->touch();
        $conversation->save();

        $message = new Message();
        $message->text = strip_tags($request->get('text'));

        $message->conversation()->associate($request->get('conversation_id'));
        $message->user()->associate(Auth::user());
        $message->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $conversation = Conversation::find($id);
        $messages = $conversation->messages()->lists('id');
        Auth::user()->seenMessages()->sync($messages->toArray(), false);

        $alreadySeen = [];
        $messages = $conversation->messages()->with('user')->orderBy('created_at', 'desc')->take(50)->get();
        foreach($messages as $key=>$message) {
            $seen = $message->seenBy()->lists('id')->toArray();
            $usersHere = array_diff($seen, $alreadySeen);
            $alreadySeen = array_merge($alreadySeen, $usersHere);
            $messages[$key]->seenUsers = User::whereIn('id', $usersHere)->get();
        }
        $messages = $messages->reverse();

        $timezoneName = timezone_name_from_abbr("", Auth::user()->timezone_offset*3600, false);

        return view('conversation/show', [
            'conversation' => $conversation,
            'timezone' => $timezoneName,
            'messages' => $messages
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
