<?php

namespace Multiweaver\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Fluent;
use Multiweaver\AmazonAPI;
use Multiweaver\Game;
use Multiweaver\Http\Requests;
use Multiweaver\Http\Controllers\Controller;
use Image;
use Multiweaver\Platform;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
	    $qb = Game::where('type', 'game')->where('multiplayer', 1)->orderBy('popularity', 'desc')->orderBy('name', 'asc');
	    if ($request->get('search')) {
		    $qb->where('name', 'like', '%'.$request->get('search').'%');
	    }
	    $paginator = $qb->paginate(20);
	    if ($request->get('search')) {
		    $paginator->appends(['search' => $request->input('search')]);
	    }
	    $paginator->setPath('games');

        return view('game/index', [
	        'games' => $paginator
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('game/create', [
		    'platforms' => Platform::all()
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
	    $data = $request->all();
	    $data['description'] = nl2br(strip_tags($data['description']));
	    $data['multiplayer'] = 1;
	    $data['type'] = 'game';
        $game = Game::create($data);

		$this->parseImageFields($game, $request);

	    $game->save();

	    $game->platforms()->detach();
	    foreach($request->get('platform') as $platform_id) {
		    $game->platforms()->attach($platform_id);
	    }

	    return redirect('games/'.$game->id);
    }

	private function parseImageFields(&$game, Request $request) {
		$imageFields = ['logo', 'banner'];
		foreach($imageFields as $field) {
			$urlFieldName = $field.'_url';
			$logo = null;
			if ($request->get($urlFieldName)) {
				$game->$urlFieldName = $request->get($urlFieldName);
				$logo = file_get_contents($game->$urlFieldName);
			} else if ($request->hasFile($field) && $request->file($field)->isValid()) {
				$logo = file_get_contents($request->file($field)->getRealPath());
			}

			if ($logo) {
				$img = (string)Image::make($logo)->encode('png');
				$unique = uniqid();
				$imageId = 'game/' . $unique . '.png';
				Storage::put($imageId, $img);
				$game->$urlFieldName = $imageId;
			}
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
	    $game = Game::findOrFail($id);
	    $games = (array)$game->users()->get();

	    $gameUsers = array_slice(array_pop($games), 0, 5);
		if (!$gameUsers) $gameUsers = [];

	    $parts = preg_split('/[^a-zA-Z0-9]+/', $game->name);
	    $qb = DB::table('games');
	    $score = '';
	    foreach($parts as $part) {
		    $score .= " + IF(name LIKE '%$part%', 1, 0)";
	    }
	    $score = trim($score, " +");
	    $qb->selectRaw('games.*, '.$score.' AS score')->where('games.steam_appid', null)->where('games.id', '<>', $game->id)->orderBy('score', 'desc');
	    $qb->take(6);
	    $duplicates = $qb->get();

	    $amazonHint = (new AmazonAPI())->doQuery($game->name);

        return view('game/show', [
	        'amazonHint' => $amazonHint,
	        'duplicates' => $duplicates,
	        'game' => $game,
	        'gameUsers' => $gameUsers
        ]);
    }

	public function merge($id, $merge) {
		if (Auth::user()->role == 'moderator') {
			$game = Game::find($id);
			$mergeable = Game::find($merge);

			foreach ($mergeable->users()->get() as $user) {
				if (!$user->games->contains($game->id)) {
					$user->games()->attach($game->id);
				}
				$user->games()->detach($mergeable->id);

				$user->save();
			}
			$mergeable->delete();
		}
		return redirect('games/'.$game->id);
	}

	public function play(Request $request, $id) {
		$game = Game::findOrFail($id);
		if (Auth::user()->games->contains($game)) {
			Auth::user()->games()->detach($game);
		} else {
			if($request->get('platform')) {
				$platform = Platform::find($request->get('platform'));
				Auth::user()->games()->attach($game, ['platform_id'=>$platform->id, 'platform_name'=>$platform->name]);
			} else {
				Auth::user()->games()->attach($game);
			}
		}

		return redirect()->back();
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		return view('game/edit', [
			'game' => Game::find($id),
			'platforms' => Platform::all()
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
	    if (Auth::check()) {
		    $game = Game::find($id);

		    $data = $request->all();

		    $game->name = $data['name'];
		    $game->description = strip_tags($data['description']);

		    $this->parseImageFields($game, $request);

		    $game->save();

		    $game->platforms()->detach();
		    $game->save();
		    if ($request->get('platform')) {
			    foreach ($request->get('platform') as $platform_id) {
				    $game->platforms()->attach($platform_id);
			    }
		    }
	    }

		return redirect('games/' . $game->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
		if (Auth::user()->role == 'moderator') {
			Game::find($id)->delete();
			if ($request->input('backTo')) {
				return redirect($request->input('backTo'));
			}
			return redirect()->back();
		}
    }
}
