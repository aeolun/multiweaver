<?php

namespace Multiweaver\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Flysystem\FileNotFoundException;
use Storage;
use Image;
use Multiweaver\Http\Requests;
use Multiweaver\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($size, $type, $id)
    {
	    $fullId = $size.'/'.$type.'/'.$id;

		try {
			$item = Storage::get($id);
		} catch(\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
			$item = Storage::disk('public')->get('no_image.png');
		}
	    $image = Image::make($item);
	    if ($size == 'original') {
			//do not do crap with the image
	    } else {
		    $size = explode('x', $size);
		    if ($type == 'fit') {
			    $image->fit($size[0], $size[1]);
		    } else if ($type == 'crop') {
			    $image->crop($size[0], $size[1]);
		    } else if ($type == 'scale') {
			    $image->resize($size[0], $size[1], function ($constraint) {
				    $constraint->aspectRatio();
			    });
		    }
	    }
	    $res = (string)$image->encode('jpg');

//	    $response->setCache([
//		    'public' => true,
//		    'max_age' => 86400 * 120
//	    ]);
        header('Content-type: image/jpeg');
        echo $res;
	    exit();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
