<?php

namespace Multiweaver\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Multiweaver\CommunicationMethod;
use Multiweaver\Conversation;
use Multiweaver\Http\Requests;
use Multiweaver\Http\Controllers\Controller;
use Multiweaver\Language;
use Multiweaver\Message;
use Multiweaver\User;
use Image;
use Storage;
use DateTimeZone;
use DateTime;

class UserController extends Controller
{
	function _getTimeZones() {
		return array_flip([
			'(GMT-11:00) Midway Island' => 'Pacific/Midway',
			'(GMT-11:00) Samoa' => 'Pacific/Samoa',
			'(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
			'(GMT-09:00) Alaska' => 'US/Alaska',
			'(GMT-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles',
			'(GMT-08:00) Tijuana' => 'America/Tijuana',
			'(GMT-07:00) Arizona' => 'US/Arizona',
			'(GMT-07:00) Chihuahua' => 'America/Chihuahua',
			'(GMT-07:00) La Paz' => 'America/Chihuahua',
			'(GMT-07:00) Mazatlan' => 'America/Mazatlan',
			'(GMT-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
			'(GMT-06:00) Central America' => 'America/Managua',
			'(GMT-06:00) Central Time (US &amp; Canada)' => 'US/Central',
			'(GMT-06:00) Guadalajara' => 'America/Mexico_City',
			'(GMT-06:00) Mexico City' => 'America/Mexico_City',
			'(GMT-06:00) Monterrey' => 'America/Monterrey',
			'(GMT-06:00) Saskatchewan' => 'Canada/Saskatchewan',
			'(GMT-05:00) Chicago' => 'America/Chicago',
			'(GMT-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern',
			'(GMT-05:00) Indiana (East)' => 'US/East-Indiana',
			'(GMT-05:00) Lima' => 'America/Lima',
			'(GMT-05:00) Quito' => 'America/Bogota',
			'(GMT-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic',
			'(GMT-04:30) Caracas' => 'America/Caracas',
			'(GMT-04:00) La Paz' => 'America/La_Paz',
			'(GMT-04:00) New York' => 'America/New_York',
			'(GMT-04:00) Santiago' => 'America/Santiago',
			'(GMT-03:30) Newfoundland' => 'Canada/Newfoundland',
			'(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
			'(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Greenland' => 'America/Godthab',
			'(GMT-03:00) Halifax' => 'America/Halifax',
			'(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
			'(GMT-01:00) Azores' => 'Atlantic/Azores',
			'(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
			'(GMT+00:00) Casablanca' => 'Africa/Casablanca',
			'(GMT+00:00) Edinburgh' => 'Europe/London',
			'(GMT+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich',
			'(GMT+00:00) Lisbon' => 'Europe/Lisbon',
			'(GMT+00:00) London' => 'Europe/London',
			'(GMT+00:00) Monrovia' => 'Africa/Monrovia',
			'(GMT+00:00) UTC' => 'UTC',
			'(GMT+01:00) Amsterdam' => 'Europe/Amsterdam',
			'(GMT+01:00) Belgrade' => 'Europe/Belgrade',
			'(GMT+01:00) Berlin' => 'Europe/Berlin',
			'(GMT+01:00) Bern' => 'Europe/Berlin',
			'(GMT+01:00) Bratislava' => 'Europe/Bratislava',
			'(GMT+01:00) Brussels' => 'Europe/Brussels',
			'(GMT+01:00) Budapest' => 'Europe/Budapest',
			'(GMT+01:00) Copenhagen' => 'Europe/Copenhagen',
			'(GMT+01:00) Ljubljana' => 'Europe/Ljubljana',
			'(GMT+01:00) Madrid' => 'Europe/Madrid',
			'(GMT+01:00) Paris' => 'Europe/Paris',
			'(GMT+01:00) Prague' => 'Europe/Prague',
			'(GMT+01:00) Rome' => 'Europe/Rome',
			'(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
			'(GMT+01:00) Skopje' => 'Europe/Skopje',
			'(GMT+01:00) Stockholm' => 'Europe/Stockholm',
			'(GMT+01:00) Vienna' => 'Europe/Vienna',
			'(GMT+01:00) Warsaw' => 'Europe/Warsaw',
			'(GMT+01:00) West Central Africa' => 'Africa/Lagos',
			'(GMT+01:00) Zagreb' => 'Europe/Zagreb',
			'(GMT+02:00) Athens' => 'Europe/Athens',
			'(GMT+02:00) Bucharest' => 'Europe/Bucharest',
			'(GMT+02:00) Cairo' => 'Africa/Cairo',
			'(GMT+02:00) Harare' => 'Africa/Harare',
			'(GMT+02:00) Helsinki' => 'Europe/Helsinki',
			'(GMT+02:00) Istanbul' => 'Europe/Istanbul',
			'(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
			'(GMT+02:00) Kyiv' => 'Europe/Helsinki',
			'(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
			'(GMT+02:00) Riga' => 'Europe/Riga',
			'(GMT+02:00) Sofia' => 'Europe/Sofia',
			'(GMT+02:00) Tallinn' => 'Europe/Tallinn',
			'(GMT+02:00) Vilnius' => 'Europe/Vilnius',
			'(GMT+03:00) Baghdad' => 'Asia/Baghdad',
			'(GMT+03:00) Kuwait' => 'Asia/Kuwait',
			'(GMT+03:00) Minsk' => 'Europe/Minsk',
			'(GMT+03:00) Nairobi' => 'Africa/Nairobi',
			'(GMT+03:00) Riyadh' => 'Asia/Riyadh',
			'(GMT+03:00) Volgograd' => 'Europe/Volgograd',
			'(GMT+03:30) Tehran' => 'Asia/Tehran',
			'(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
			'(GMT+04:00) Baku' => 'Asia/Baku',
			'(GMT+04:00) Moscow' => 'Europe/Moscow',
			'(GMT+04:00) Muscat' => 'Asia/Muscat',
			'(GMT+04:00) St. Petersburg' => 'Europe/Moscow',
			'(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
			'(GMT+04:00) Yerevan' => 'Asia/Yerevan',
			'(GMT+04:30) Kabul' => 'Asia/Kabul',
			'(GMT+05:00) Islamabad' => 'Asia/Karachi',
			'(GMT+05:00) Karachi' => 'Asia/Karachi',
			'(GMT+05:00) Tashkent' => 'Asia/Tashkent',
			'(GMT+05:30) Chennai' => 'Asia/Calcutta',
			'(GMT+05:30) Kolkata' => 'Asia/Kolkata',
			'(GMT+05:30) Mumbai' => 'Asia/Calcutta',
			'(GMT+05:30) New Delhi' => 'Asia/Calcutta',
			'(GMT+05:30) Sri Jayawardenepura' => 'Asia/Calcutta',
			'(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
			'(GMT+06:00) Almaty' => 'Asia/Almaty',
			'(GMT+06:00) Astana' => 'Asia/Dhaka',
			'(GMT+06:00) Dhaka' => 'Asia/Dhaka',
			'(GMT+06:00) Ekaterinburg' => 'Asia/Yekaterinburg',
			'(GMT+06:30) Rangoon' => 'Asia/Rangoon',
			'(GMT+07:00) Bangkok' => 'Asia/Bangkok',
			'(GMT+07:00) Hanoi' => 'Asia/Bangkok',
			'(GMT+07:00) Jakarta' => 'Asia/Jakarta',
			'(GMT+07:00) Novosibirsk' => 'Asia/Novosibirsk',
			'(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
			'(GMT+08:00) Chongqing' => 'Asia/Chongqing',
			'(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
			'(GMT+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
			'(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
			'(GMT+08:00) Perth' => 'Australia/Perth',
			'(GMT+08:00) Singapore' => 'Asia/Singapore',
			'(GMT+08:00) Taipei' => 'Asia/Taipei',
			'(GMT+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator',
			'(GMT+08:00) Urumqi' => 'Asia/Urumqi',
			'(GMT+09:00) Irkutsk' => 'Asia/Irkutsk',
			'(GMT+09:00) Osaka' => 'Asia/Tokyo',
			'(GMT+09:00) Sapporo' => 'Asia/Tokyo',
			'(GMT+09:00) Seoul' => 'Asia/Seoul',
			'(GMT+09:00) Tokyo' => 'Asia/Tokyo',
			'(GMT+09:30) Adelaide' => 'Australia/Adelaide',
			'(GMT+09:30) Darwin' => 'Australia/Darwin',
			'(GMT+10:00) Brisbane' => 'Australia/Brisbane',
			'(GMT+10:00) Canberra' => 'Australia/Canberra',
			'(GMT+10:00) Guam' => 'Pacific/Guam',
			'(GMT+10:00) Hobart' => 'Australia/Hobart',
			'(GMT+10:00) Melbourne' => 'Australia/Melbourne',
			'(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby',
			'(GMT+10:00) Sydney' => 'Australia/Sydney',
			'(GMT+10:00) Yakutsk' => 'Asia/Yakutsk',
			'(GMT+11:00) Vladivostok' => 'Asia/Vladivostok',
			'(GMT+12:00) Auckland' => 'Pacific/Auckland',
			'(GMT+12:00) Fiji' => 'Pacific/Fiji',
			'(GMT+12:00) International Date Line West' => 'Pacific/Kwajalein',
			'(GMT+12:00) Kamchatka' => 'Asia/Kamchatka',
			'(GMT+12:00) Magadan' => 'Asia/Magadan',
			'(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
			'(GMT+12:00) New Caledonia' => 'Asia/Magadan',
			'(GMT+12:00) Solomon Is.' => 'Asia/Magadan',
			'(GMT+12:00) Wellington' => 'Pacific/Auckland',
			'(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
		]);
	}

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
		$play_time_from = $request->get('play_time_from') || ! Auth::user() ? $request->get('play_time_from') : Auth::user()->play_time_from;
		$play_time_to = $request->get('play_time_to') || ! Auth::user() ? $request->get('play_time_to') : Auth::user()->play_time_to;

		$ids = [];
		if ($request->get('communication')) {
			foreach($request->get('communication') as $id) {
				$ids[] = $id;
			}
		} else if (Auth::check()) {
			foreach(Auth::user()->communication_methods()->get() as $method) {
				$ids[] = $method->id;
			}
		}

		$language_ids = [];
		if ($request->get('language')) {
			foreach($request->get('language') as $id) {
				$language_ids[] = $id;
			}
		} else if (Auth::check()) {
			foreach(Auth::user()->languages()->get() as $lang) {
				$language_ids[] = $lang->id;
			}
		}

		$timezone_offset = null;
		if($request->get('timezone_offset')) {
			$timezone_offset = $request->get('timezone_offset');
		} else if (Auth::check()) {
			$timezone_offset = Auth::user()->timezone_offset;
		}

		$looking_for = null;
		if($request->get('looking_for')) {
			$looking_for = $request->get('looking_for');
		} else if (Auth::check()) {
			$looking_for = Auth::user()->looking_for;
		}

		$mature = null;
		if($request->get('mature')) {
			$mature = $request->get('mature');
		} else if (Auth::check()) {
			$mature = Auth::user()->maturity;
		}

		$my_games = false;
		if (Auth::check() && !$request->get('looking_for')) {
			$my_games = true;
		} else if ($request->get('my_games')) {
			$my_games = true;
		}

		$users = User::select('users.*')->join('game_user', 'users.id', '=', 'game_user.user_id')->groupBy('users.id');
	    if (Auth::check()) {
		    $users->where('users.id', '<>', Auth::user()->id);
	    }
	    if ($request->get('game')) {
		    $users = $users->join('games', 'game_user.game_id', '=', 'games.id');
		    $users = $users->where('games.name', 'like', '%'.$request->get('game').'%');
	    }

		if ($looking_for) {
			$users = $users->where('users.looking_for', $looking_for);
		}

	    if ($timezone_offset) {
		    $gmt_time =  - $timezone_offset;
		    $gmt_time_from = $gmt_time > 24 ? $gmt_time - 24 : $gmt_time < 0 ? $gmt_time + 24 : $gmt_time;

		    $gmt_time = $request->get('play_time_to') - $timezone_offset;
		    $gmt_time_to = $gmt_time > 24 ? $gmt_time - 24 : $gmt_time < 0 ? $gmt_time + 24 : $gmt_time;

		    $gmt_time_from = intval($gmt_time_from);
		    $gmt_time_to = intval($gmt_time_to);

			$users = $users->whereRaw('(('.$gmt_time_to.' < '.$gmt_time_from.' AND ('.$gmt_time_to.' > gmt_time_from OR '.$gmt_time_from.' < gmt_time_to)) OR (gmt_time_from < gmt_time_to AND ('.$gmt_time_to.' > gmt_time_from AND '.$gmt_time_from.' < gmt_time_to)) OR (gmt_time_from >= gmt_time_to AND ('.$gmt_time_to.' < gmt_time_to OR '.$gmt_time_from.' >= gmt_time_from)))');
	    }
		if (count($ids)) {
			$users = $users->join('communication_method_user', 'communication_method_user.user_id', '=', 'users.id');
			$users = $users->whereIn('communication_method_user.communication_method_id', $ids);
		}

		if(count($language_ids)) {
			$users = $users->join('language_user', 'language_user.user_id', '=', 'users.id');
			$users = $users->whereIn('language_user.language_id', $language_ids);
		}
		if ($mature) {
			$users->where('users.birthdate', '<', date('Y-m-d', time()-86400*365*21));
		}
		if ($my_games) {
			$gameIds = Auth::user()->games()->lists('id');

			$users = $users->join('games', 'game_user.game_id', '=', 'games.id');
			$users = $users->whereIn('games.id', $gameIds);
		}

	    $users->orderBy('random_order');
	    $pagination = $users->paginate(18);
	    $pagination->appends($request->all());
	    $pagination->setPath('users');

	    return view('user/index', [
		    'users' => $pagination,
		    'timezones' => $this->_getTimeZones(),
		    'gmt' => isset($gmt_time_from) ? [$gmt_time_from, $gmt_time_to] : null,
		    'languages' => Language::all(),
		    'communication_methods' => CommunicationMethod::all(),
		    'looking_for' => [
			    'casual' => 'Casual',
			    'competitive' => 'Competitive'
		    ],
		    'filter' => [
				'communication' => $ids,
				'language' => $language_ids,
				'play_time_from' => $play_time_from,
				'play_time_to' => $play_time_to,
				'timezone_offset' => $timezone_offset,
				'looking_for' => $looking_for,
				'mature' => $mature,
				'my_games' => $my_games
			]
	    ]);
    }

	/**
	 * Show a list of suggested playing partners for the user.
	 *
	 * @return Response
	 */
	public function suggest() {
		$pagination = Auth::user()->suggestions()->paginate(20);
		$pagination->setPath('suggest');

		return view('user/suggest', [
			'users' => $pagination
		]);
	}

	public function search(Request $request) {
		$users = User::select('id', 'name AS text', 'avatar', DB::raw('MD5(email) AS emailHash'))->where('name', 'like', "%".$request->get('q')."%");
		$total = $users->count();
		$users = $users->take(10)->skip(($request->get('page', 1)-1)*10)->get();

		return json_encode([
			'items' => $users,
			'total_count' => $total
		]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
	    return view('user/show', [
		    'user' => User::findOrFail($id)
	    ]);
    }

	/**
	 * Contact the specified player
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function contact($id)
	{
		return view('user/contact', [
			'user' => User::findOrFail($id)
		]);
	}

	/**
	 * Sends the given message to the player
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function send(Request $request, $id)
	{
		$user = User::findOrFail($id);
		$extra = '';
		if ($request->input('message')) {
			$extra = "\n\n".$request->input('message');
		}
		$text = view('emails/contact', [
			'user' => $user,
			'extra' => $extra
		]);
		$conversation = new Conversation();
		$conversation->title = 'Contact from '.Auth::user()->name;
		$conversation->save();
		$conversation->users()->attach($user->id);
		$conversation->users()->attach(Auth::user()->id);

		$message = new Message();
		$message->conversation()->associate($conversation);
		$message->user()->associate(Auth::user());
		$message->text = $text;
		$message->save();

		return redirect(url('conversations#'.$conversation));
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        //
	    $timezones = $this->_getTimeZones();
	    $user = User::findOrFail($id);
	    return view('user/edit', [
		    'user' => $user,
		    'timezones' => $timezones,
		    'languages' => Language::all(),
		    'communication_methods' => CommunicationMethod::all(),
		    'looking_for' => [
			    'casual' => 'Casual',
			    'competitive' => 'Competitive'
		    ]
	    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
		$data = $request->all();

	    if(!isset($data['maturity'])) $data['maturity'] = 0;
	    if(isset($data['about'])) $data['about'] = strip_tags($data['about']);
		if(isset($data['password']) && $data['password'] && $data['password'] == $data['password_confirmation']) {
			$data['password'] = bcrypt($data['password']);
		} else {
			unset($data['password']);
		}
	    if ($data['play_time']) {
		    list($from, $to) = explode(',', $data['play_time']);
			$data['play_time_from'] = $from;
		    $data['play_time_to'] = $to;
	    }
	    $user->update($data);

	    $user->timezone = $data['timezone'];
	    $user->timezone_offset = Carbon::now($data['timezone'])->offsetHours;

	    $user->gmt_time_from = (new Carbon($user->play_time_from.':00:00', new DateTimeZone($user->timezone)))->setTimeZone('GMT')->format('H');
	    $user->gmt_time_to = (new Carbon($user->play_time_to.':00:00', new DateTimeZone($user->timezone)))->setTimeZone('GMT')->format('H');

		$user->save();

	    if ($request->hasFile('avatar')) {
		    $image = file_get_contents($request->file('avatar')->getPathname());
		    $img = (string)Image::make($image)->encode('png');
			$imageId = 'avatar/' . uniqid() . '.png';
		    Storage::put($imageId, $img);
			$user->avatar = $imageId;
			$user->save();
	    }

	    Auth::user()->languages()->detach();
	    if ($request->get('language')) {
		    foreach ($request->get('language') as $language) {
			    $lang = Language::findOrFail($language);
			    Auth::user()->languages()->attach($lang);
		    }
	    }
	    Auth::user()->communication_methods()->detach();
	    if ($request->get('method')) {
		    foreach ($request->get('method') as $method) {
			    $meth = CommunicationMethod::findOrFail($method);
			    Auth::user()->communication_methods()->attach($meth);
		    }
	    }
	    $request->session()->flash('status', 'Updated!');

	    return redirect(url('users/'.$user->id.'/edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
