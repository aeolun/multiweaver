<?php

namespace Multiweaver\Http\Controllers\Auth;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Multiweaver\EmailLogin;
use Multiweaver\User;
use Socialite;
use Validator;
use Multiweaver\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	var $redirectPath = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout', 'getCode']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

	public function getCode(Request $request, $code)
	{
		$userLogin = EmailLogin::where('unique_id', $code)->where('expires', '>', Carbon::now())->first();
		if ($userLogin) {
			if (!Auth::check()) Auth::loginUsingId($userLogin->user_id);

			if($userLogin->redirect) {
				return redirect($userLogin->redirect);
			}
			return redirect(url(''));
		}
		return redirect(url('auth/login'));
	}

	public function steam() {
        $key = '4E7F562A8F67A9FDBCA9C7D9E0B7EFB3';
		$openid = new \LightOpenID(url('auth/steam'));
		if(!$openid->mode) {
			$openid->identity = 'http://steamcommunity.com/openid/?l=english';    // This is forcing english because it has a weird habit of selecting a random language otherwise
			return redirect($openid->authUrl());
		} elseif($openid->mode == 'cancel')
        {
            echo 'User has canceled authentication!';
        }
        else
        {
            if($openid->validate())
            {
                $id = $openid->identity;
                // identity is something like: http://steamcommunity.com/openid/id/76561197960435530
                // we only care about the unique account ID at the end of the URL.
                $ptn = "/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
                preg_match($ptn, $id, $matches);
                $steamId = $matches[1];

                $user = User::where('steam_id', $steamId)->first();
                if (!$user) {
                    $url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$key&steamids=$steamId";
                    $json_object= file_get_contents($url);
                    $json_decoded = json_decode($json_object);

                    $data = $json_decoded->response->players[0];

                    $user = new User;
                    $user->name = $data->personaname;
                    $user->steam_id = $steamId;
                    $user->email = $data->personaname;

                    if ($data->avatarfull) {
                        $image = file_get_contents($data->avatarfull);
                        $img = (string)Image::make($image)->encode('png');
                        $imageId = 'avatar/' . uniqid() . '.png';
                        Storage::put($imageId, $img);
                        $user->avatar = $imageId;
                    }
                    $res = $user->save();
                }
                Auth::loginUsingId($user->id);
                return redirect(url('users/'.$user->id.'/edit'));
            }
            else
            {
                echo "User is not logged in.\n";
            }
        }
	}

	/**
	 * Redirect the user to the GitHub authentication page.
	 *
	 * @return Response
	 */
	public function redirectToProvider()
	{
		return Socialite::driver('google')->scopes([
			'profile',
			'email'
		])->redirect();
	}

	/**
	 * Obtain the user information from GitHub.
	 *
	 * @return Response
	 */
	public function handleProviderCallback()
	{
		$google = Socialite::driver('google')->user();

		$user = User::where('google_id', $google->getId())->first();
		if ($user) {
			Auth::loginUsingId($user->id);
		} else {
			$user = new User;
			$user->name = $google->getNickname()?$google->getNickname():$google->getName();
			$user->email = $google->getEmail();
			$user->google_id = $google->getId();
			$user->save();
			Auth::loginUsingId($user->id);
		}

		return redirect('/');
	}
}
