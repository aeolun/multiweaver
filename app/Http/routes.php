<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Multiweaver\Game;

Route::get('/', function () {
    $items = Game::where('banner_url', '<>', '')->lists('id');
    $eligibleGames = $items->all();
    shuffle($eligibleGames);
    $eligibleGames = array_splice($eligibleGames, 0, 5);

    $games = Game::whereIn('id', $eligibleGames)->get();
    return view('welcome', [
        'carousel' => $games
    ]);
});

Route::get('games/play/{id}', 'GameController@play');
Route::get('games/{id}/merge/{merge}', 'GameController@merge');

Route::get('users/{id}/contact', 'UserController@contact');
Route::post('users/{id}/send', 'UserController@send');
Route::get('users/{id}/suggest', 'UserController@suggest');

Route::get('ajax/search', 'UserController@search');
Route::get('conversations/{id}/watch', 'ConversationController@watch');
Route::get('conversations/{id}/add/{user}', 'ConversationController@add');
Route::post('conversations/{id}/message', 'ConversationController@message');

Route::resource('games', 'GameController');
Route::resource('users', 'UserController');
Route::resource('conversations', 'ConversationController');

Route::get('login/{code}', 'Auth\AuthController@getCode');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('auth/google', 'Auth\AuthController@redirectToProvider');
Route::get('auth/google/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('auth/steam', 'Auth\AuthController@steam');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('image/{size}/{type}/{id}', 'ImageController@show')->where('id', '.*');