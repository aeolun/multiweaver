<?php

namespace Multiweaver;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $fillable = ['name'];
}
