<?php
/**
 * Created by PhpStorm.
 * User: aeolu
 * Date: 08/04/2016
 * Time: 21:02
 */

namespace Multiweaver;

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;
use ApaiIO\ResponseTransformer\ObjectToArray;
use ApaiIO\ResponseTransformer\ResponseTransformerFactory;
use ApaiIO\ResponseTransformer\XmlToSimpleXmlObject;

class AmazonAPI
{
	private $accessKey = 'AKIAJO7VHRL5524V6YXA';
	private $accessSecret = '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';
	private $associateTag = 'multiweaver-20';

	function doQuery($game) {
		$conf = new GenericConfiguration();
		$conf
			->setCountry('com')
			->setAccessKey($this->accessKey)
			->setSecretKey($this->accessSecret)
			->setResponseTransformer(XmlToSimpleXmlObject::class)
			->setAssociateTag($this->associateTag);

		$apaiIO = new ApaiIO($conf);

		$search = new Search();
		$search->setCategory('Software');
		$search->setTitle($game);

		$formattedResponse = $apaiIO->runOperation($search);

		if (!isset($formattedResponse->Items->Item[0])) return null;

		$asin = $formattedResponse->Items->Item[0]->ASIN;
		$title = $formattedResponse->Items->Item[0]->ItemAttributes->Title;
		$detailPageUrl = $formattedResponse->Items->Item[0]->DetailPageURL;

		$lookup = new Lookup();
		$lookup->setItemId($asin);
		$lookup->setResponseGroup(['Images', 'Offers']);

		$moreInfo = $apaiIO->runOperation($lookup);

		if (!isset($formattedResponse->Items->Item[0])) return null;

		$smallImage = $moreInfo->Items->Item[0]->SmallImage->URL;
		$mediumImage = $moreInfo->Items->Item[0]->MediumImage->URL;
		$largeImage = $moreInfo->Items->Item[0]->LargeImage->URL;

		$price = $moreInfo->Items->Item[0]->OfferSummary->LowestNewPrice->FormattedPrice;

		return [
			'asin' => $asin,
			'title' => $title,
			'url' => $detailPageUrl,
			'smallImage' => $smallImage,
			'mediumImage' => $mediumImage,
			'largeImage' => $largeImage,
			'price' => $price
		];
	}
}