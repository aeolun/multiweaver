<?php

namespace Multiweaver;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    function games() {
        return $this->belongsToMany('\Multiweaver\Game');
    }
}
