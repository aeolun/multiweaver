<?php

namespace Multiweaver;

use Illuminate\Database\Eloquent\Model;

class Screenshot extends Model
{
	protected $fillable = ['url', 'game_id', 'original_path'];
}
