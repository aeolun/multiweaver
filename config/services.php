<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '71uyXvIOpFj6oRF_fpgygg',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => Multiweaver\User::class,
        'key'    => '',
        'secret' => '',
    ],

	'google' => [
		'client_id' => '1009609752364-uuobrod0kbuu38ekhptq5q6u025svdvb.apps.googleusercontent.com',
		'client_secret' => 'syomxQqcHIXow-wJz_c6iIz2',
		'redirect' => env('APP_URL', 'http://www.multiweaver.com/').'auth/google/callback',
	],

];
