var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
	'bootstrap': './bower_components/bootstrap/'
}

elixir(function(mix) {
    mix.styles([
	    '../../../bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.css',
		'../../../bower_components/bootstrap-select/dist/css/bootstrap-select.css',
    ]);
	mix.sass('app.scss');
	mix.scripts([
		'../../../bower_components/jquery/dist/jquery.js',
		'../../../bower_components/tether/dist/js/tether.js',
		'../../../bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.js',
		'../../../bower_components/bootstrap/dist/js/bootstrap.js',
		'../../../bower_components/bootstrap-select/dist/js/bootstrap-select.js',
		'app.js',
		'conversations.js',
	])
});
