<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    Login | Multiweaver
@endsection

@section('content')
    <div class="row" style="margin:80px 0px;">
        <div class="col-md-2"></div>
        <div class="col-md-8 alert alert-info">
            <h1>Log in</h1>
    <form class="signup-form form-horizontal" method="POST" action="{{ url('auth/login') }}">
        {!! csrf_field() !!}

        <div class="form-group row">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8"><input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Enter your email"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="password" id="password" placeholder="Enter your password">
                <div class="row-margin"><a href="{{ url('password/email') }}">I forgot my password!</a></div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                <label><input type="checkbox" name="remember"> Remember Me</label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
            <button class="btn btn-primary" type="submit">Login</button>
                <a href="{{ url('auth/google') }}"><img src="{{ asset('images/signin_with_google.png') }}" height="42px" /></a>
                <a href="{{ url('auth/steam') }}"><img src="{{ asset('images/sign_in_through_steam.png') }}" height="36px" /></a>
            </div>
        </div>
    </form>
    </div>
    </div>
    </div>
@endsection