<!-- resources/views/auth/password.blade.php -->
@extends('layout.layout')

@section('title')
    Reset Password | Multiweaver
@endsection

@section('content')
    <h1>Reset Password</h1>
    <form class="signup-form form-horizontal" method="POST" action="{{ url('password/email') }}">
        {!! csrf_field() !!}

        <div class="form-group row">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit">Send Password Reset Link</button>
            </div>
        </div>
    </form>
@endsection