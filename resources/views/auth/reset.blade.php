<!-- resources/views/auth/reset.blade.php -->
@extends('layout.layout')

@section('title')
    Reset Password | Multiweaver
@endsection

@section('content')
    <h1>Enter New Password</h1>
    <form method="POST" action="{{ url('password/reset') }}">
        {!! csrf_field() !!}
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group row">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="password">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 control-label">Password Confirmation</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="password_confirmation">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit">Reset Password</button>
            </div>
        </div>
    </form>
@endsection