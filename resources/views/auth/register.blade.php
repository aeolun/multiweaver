<!-- resources/views/auth/register.blade.php -->
@extends('layout.layout')

@section('title')
    Create Account | Multiweaver
@endsection

@section('content')
    <div class="row" style="margin:80px 0px;">
    <div class="col-md-2"></div>
    <div class="col-md-8 alert alert-info">
    <h1>Start Finding People To Game With!</h1>
    <form class="signup-form form-horizontal row-margin" method="POST" action="{{ url('auth/register') }}">
        {!! csrf_field() !!}

        <div class="form-group row">
            <label class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8"><input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Enter the name you'd like to show to other users"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8"><input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="We use your email to send you notifications"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8"><input class="form-control" type="password" name="password" placeholder="Minimum of 6 characters. But for your own sake, use a secure password"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Confirm Password</label>
            <div class="col-sm-8">
            <input class="form-control" type="password" name="password_confirmation" placeholder="Please confirm your password">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4">&nbsp;</div>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit">Register</button> <a href="{{ url('auth/google') }}"><img src="{{ asset('images/signin_with_google.png') }}" height="42px" /></a>
                <a href="{{ url('auth/steam') }}"><img src="{{ asset('images/sign_in_through_steam.png') }}" height="36px" /></a>
            </div>
        </div>
    </form>
    </div>
    <div class="col-md-2"></div>
    </div>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1488762318100368');
        fbq('track', "PageView");
        </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1488762318100368&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
@endsection