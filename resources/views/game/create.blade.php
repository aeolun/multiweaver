<!-- resources/views/auth/register.blade.php -->
@extends('layout.layout')

@section('title')
    Add Game | Multiweaver
@endsection

@section('content')
    <h1>Add a Game you want to Play</h1>
    <form class="signup-form form-horizontal" method="POST" action="{{ url('games') }}">
        {!! csrf_field() !!}

        <div class="form-group row">
            <label class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="name" placeholder="The name of the game" value="{{ old('name') }}">
                <div class="text-danger row-margin">
                    <h2>DID YOU CHECK WHETHER THIS GAME ALREADY EXISTS?</h2>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Description</label>
            <div class="col-sm-8"><textarea rows="4" class="form-control" name="description" placeholder="Enter a short description of this game">{{ old('description') }}</textarea></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Logo Url</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input class="form-control" type="text" name="logo_url" placeholder="Image URL" onchange="$('.logo-preview').attr('src', this.value);">
                    <span class="input-group-addon"><img class="logo-preview" src="{{ asset('images/logo.png') }}" height="20" /></span>
                </div>
                <div class="help-block text-muted row-margin">Something around 400x200px works best. It'll work with anything, it'll just look ugly as hell.</div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Available Platforms</label>
            <div class="col-sm-8">
                @foreach($platforms as $platform)
                    <div class="col-sm-12 col-md-6 checkbox">
                        <label>
                            <input type="checkbox" value="{{ $platform->id }}" name="platform[]" /> {{ $platform->name }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4">&nbsp;</div>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit">Create</button>
            </div>
        </div>
    </form>
@endsection