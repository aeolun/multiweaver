<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    {{ $game->name }} | Multiweaver
@overwrite

@section('head')
    <meta property="og:type" content="game" />
    <meta property="og:image" content="{{ $game->screenshots->count() > 0 ? url('image/original/original/'.$game->screenshots->first()->url) : ($game->banner_url ? url('image/1110x362/fit/'.$game->banner_url) : url('image/460x215/scale/'.$game->logo_url)) }}" />
    <meta property="og:description" content="{{ strip_tags($game->description) }}" />
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 text-xs-center">
            @if($game->banner_url)
                <img src="{{ url('image/1110x362/fit/'.$game->banner_url) }}" alt="{{ $game->name }}">
            @else
                <img style="max-width:100%;" src="{{ url('image/460x215/scale/'.$game->logo_url) }}" />
            @endif
        </div>
        <div class="col-md-12 row-margin">
            <h1>{{ $game->name }}</h1>
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-8">
            <div class="row">
                @foreach($game->screenshots()->get() as $idx => $screenshot)
                    <div class="col-md-3 row-margin">
                        <a href="{{ url('image/original/original/'.$screenshot->url) }}" class="pop"><img class="img-fluid" src="{{ url('image/200x150/scale/'.$screenshot->url) }}" /></a>
                    </div>
                    @if($idx == 3)
                        <div class="col-md-12 row-margin">
                            <a class="btn btn-secondary" onclick="$(this).parent().next().show();$(this).parent().remove();">Show More Screenshots</a>
                        </div>
                        <div id="rest_images" class="col-md-12" style="display:none;">
                            <div class="row">
                    @endif
                @endforeach
                @if($game->screenshots()->count() > 4)
                    </div></div>
                @endif
            </div>
            <div class="row row-margin">

                <div class="col-md-12">
                    <div class="glass-panel">
                    {!! Markdown::convertToHtml($game->description) !!}
                    </div>
                </div>
            </div>
            <div class="row row-margin">
                <div class="col-md-12">
                    <h2>People that want to play this game ({{ $game->users()->count() }})</h2>
                </div>
                @foreach($gameUsers as $user)
                    @include('_partials.user', [ 'size' => 4 ])
                @endforeach
                @if(count($gameUsers) == 5)
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title text-center">More Users</h4>
                                <p class="card-text">
                                    <a class="btn btn-primary btn-block" href="{{ url('users?game='.$game->name) }}">Show</a>
                                </p>
                            </div>
                        </div>

                    </div>
                @elseif(count($gameUsers) == 0)
                    <div class="col-md-12">
                        <p>Nobody interested in playing this game at the moment. Be the first!</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-4 row-margin">
            @if(Auth::user())
                @if(Auth::user()->isInterested($game))
                    <a href="{{ url('games/play/'.$game->id) }}" class="btn btn-lg btn-block btn-success"><i class="fa fa-remove"></i> Interested {{ Auth::user()->getPlatform($game) }}</a>
                @else
                    <form action="{{ url('games/play/'.$game->id) }}" method="get">
                        @if($game->platforms()->count() > 1)
                            <select name="platform" required="required" class="form-control form-control-lg">
                                <option value="">-- Select Platform --</option>
                                @foreach($game->platforms as $platform)
                                    <option value="{{ $platform->id }}">{{ $platform->name }}</option>
                                @endforeach
                            </select>
                        @endif
                        <button type="submit" style="margin-top:4px;" class="btn btn-lg btn-block btn-success-outline">Interested</button>
                    </form>
                @endif
            @else
                <a class="btn btn-lg btn-block btn-success" href="{{ url('auth/register') }}">Sign up to Add</a>
            @endif
                @if (Auth::user() && $game)
                    <a class="btn btn-lg btn-block btn-info row-margin" href="{{ url('games/'.$game->id.'/edit') }}"><i class="fa fa-pencil"></i> Edit</a>
                @endif

            @if($game->steam_appid)
                <a class="btn btn-lg btn-block btn-warning" href="http://store.steampowered.com/app/{{ $game->steam_appid }}/"><i class="fa fa-money"></i> Buy on Steam</a>
            @endif
            @if (Auth::user() && Auth::user()->role == 'moderator' && $game)
                    <div class="row-margin">

                    <form method="post" action="{{ url('games/'.$game->id) }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="_method" value="DELETE" />
                        <input type="hidden" name="backTo" value="{{ Request::server('HTTP_REFERER') }}" />
                        <button type="submit" class="btn btn-lg btn-danger btn-block"><i class="fa fa-remove"></i> Delete</button>
                    </form>
                    </div>
            @endif
                <br />
            @if($amazonHint)
                <div class="card card-danger-outline">
                    <div class="card-block">
                        <div class="text-xs-center">
                        <h3 class="card-title">Blatant Advertisement</h3>
                        <a href="{{ $amazonHint['url'] }}" rel="nofollow">
                            <p><img src="{{ $amazonHint['largeImage'] }}" class="img-fluid" /></p>
                            <div>{{ $amazonHint['title'] }}</div>
                            <h4>{{ $amazonHint['price'] }}</h4>
                        </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if(Auth::check() && Auth::user()->role == 'moderator')
        <a class="btn btn-primary" onclick="$(this).next().toggle()">Find Duplicates</a>
    <div class="row-margin" style="display:none;">
        <h1>Duplicates</h1>
        <table class="table table-striped">
            <tr>
                <th>Logo</th>
                <th>Name</th>
                <th>Popularity</th>
                <th>Description</th>
                <th>Merge</th>

            </tr>
        @foreach($duplicates as $duplicate)
            <tr>
                <td>
                    <img class="pull-left" src="{{ url('image/150x150/scale/'.$duplicate->logo_url) }}" />
                </td>
                <td><a href="{{ url('games/'.$duplicate->id) }}">{{ $duplicate->name }}</a></td>
                <td>{{ $duplicate->score }}</td>
                <td>{{ $duplicate->description }}</td>
                <td><a href="{{ url('games/'.$game->id.'/merge/'.$duplicate->id) }}" class="btn btn-primary">Merge</a></td>
            </tr>
        @endforeach
        </table>
    </div>
    @endif
            <!-- Creates the bootstrap modal where the image will appear -->
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Image preview</h4>
                </div>
                <div class="modal-body">
                    <img src="" id="imagepreview" class="img-fluid" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection