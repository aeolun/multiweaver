<!-- resources/views/auth/register.blade.php -->
@extends('layout.layout')

@section('title')
    Edit {{ $game->name }} | Multiweaver
@endsection

@section('content')
    <h1>Edit Game</h1>
    <form class="signup-form form-horizontal" method="POST" enctype="multipart/form-data" action="{{ url('games/'.$game->id) }}">
        {!! csrf_field() !!}

        <div class="form-group row">
            <label class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8"><input class="form-control" type="text" name="name" placeholder="The name of the game" value="{{ $game->name }}"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Description</label>
            <div class="col-sm-8">
                <textarea rows="6" class="form-control" name="description" placeholder="Enter a short description of this game">{{ strip_tags($game->description) }}</textarea>
                <div class="help-block row-margin text-muted">
                    You can use <a href="http://commonmark.org/help/">Markdown</a> to style the text.
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Logo Image</label>
            <div class="col-sm-8">
                @if($game->logo_url)
                    <img style="max-width:100%;" src="{{ url('image/460x215/fit/'.$game->logo_url) }}" />
                @endif
                <div class="input-group row-margin">
                    <input class="form-control" type="text" name="logo_url" placeholder="New Image URL" value="" onchange="$('.logo-preview').attr('src', this.value);">
                    <span class="input-group-addon"><img class="logo-preview" src="{{ $game->logo_url ? url('image/460x215/fit/'.$game->logo_url) : asset('images/logo.png') }}" height="20" /></span>
                </div>
                    <div>or</div>
                <div class="input-group row-margin">
                    <input class="form-control" type="file" name="logo" placeholder="New Image" value="">
                </div>
                <div class="help-block text-muted row-margin">Something around 400x200px (or around that ratio) works best. Make sure the logo is centered in the image. It'll work with anything (up to a point), it just won't look amazing.</div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Banner Image</label>
            <div class="col-sm-8">
                @if($game->banner_url)
                    <img style="max-width:100%;" src="{{ url('image/1110x362/fit/'.$game->banner_url) }}" />
                @endif
                <div class="input-group row-margin">
                    <input class="form-control" type="text" name="banner_url" placeholder="New Image URL" value="" onchange="$('.banner-preview').attr('src', this.value);">
                    <span class="input-group-addon"><img class="banner-preview" src="{{ $game->banner_url ? url('image/1110x362/fit/'.$game->banner_url) : asset('images/logo.png') }}" height="20" /></span>
                </div>
                <div>or</div>
                <div class="input-group row-margin">
                    <input class="form-control" type="file" name="banner" placeholder="New Image" value="">
                </div>
                <div class="help-block text-muted row-margin">Something around 1110x362px (or around that ratio) works best. It'll work with anything (up to a point), it just won't look amazing.</div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Available Platforms</label>
            <div class="col-sm-8">
                @foreach($platforms as $platform)
                    <div class="col-sm-12 col-md-6 checkbox">
                        <label>
                            <input type="checkbox" value="{{ $platform->id }}" name="platform[]" {{ $game->platforms->contains($platform) ? 'checked="checked"' : '' }} /> {{ $platform->name }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-4">&nbsp;</div>
            <div class="col-sm-8">
                <input type="hidden" name="_method" value="PUT" />
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
        </div>
    </form>
@endsection