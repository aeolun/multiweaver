<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')


@section('title')
    Games ({{ $games->currentPage() }}) | Multiweaver
@endsection

@section('content')
    <h1>Games</h1>
    <nav class="navbar navbar-light bg-glass navbar-search">
        <ul class="nav navbar-nav pull-right">
            <li class="nav-item">
                @if(Auth::check())
                    <a href="{{ url('games/create') }}" class="btn btn-primary" href="#">Add a Game you want to Play</a>
                @else
                    <a href="{{ url('auth/register') }}" class="btn btn-primary" href="#">Sign up to add a game</a>
                @endif

            </li>
        </ul>
        <form class="form-inline">
            <input class="form-control" type="search" name="search" placeholder="Search" value="{{ Request::input('search') }}">
            <button class="btn btn-primary-outline" type="submit">Search</button>
        </form>
    </nav>
    <div class="row row-margin">
        @foreach($games as $game)
            @include('_partials.game')
        @endforeach
    </div>
    <div class="text-center glass-panel">
        @include('pagination.default', ['paginator' => $games])
    </div>
    <div class="text-center">
        @if(Auth::user())
            <a href="{{ url('games/create') }}" class="btn btn-lg btn-primary row-margin">Add a Game you want to Play</a>
        @else
            <a href="{{ url('auth/register') }}" class="btn btn-lg btn-primary row-margin">Sign up to add a game</a>
        @endif
    </div>

@endsection