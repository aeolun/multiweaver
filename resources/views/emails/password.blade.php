@extends('emails._layout')

@section('content')
    <table class="one-col centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;table-layout: fixed;background-color: #fefefe;">
        <tbody><tr>
            <td class="column" style="padding: 0;vertical-align: top;text-align: left;">
                <div><div class="column-top" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div></div>
                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <h1 class="size-30" style="font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-size: 30px;line-height: 38px;font-family: Merriweather,Georgia,serif;color: #353638;text-align: center;">You have requested a password reset!</h1>
                            <p class="size-17" style="font-style: normal;font-weight: 400;Margin-bottom: 27px;Margin-top: 24px;font-size: 17px;line-height: 26px;font-family: Merriweather,Georgia,serif;color: #353638;text-align: center;">
                            If you have not requested this password reset, please ignore this email.
                                </p>
                        </td>
                    </tr>
                    </tbody></table>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <div class="btn" style="Margin-bottom: 0;Margin-top: 0;text-align: center;">
                                <!--[if !mso]--><a style="border-radius: 3px;display: inline-block;font-size: 14px;font-weight: 700;line-height: 24px;padding: 13px 35px 12px 35px;text-align: center;text-decoration: none !important;transition: opacity 0.2s ease-in;color: #fff;font-family: Merriweather,Georgia,serif;background-color: #70717d;" href="{{ url('password/reset/'.$token) }}" data-width="72" target="_blank">Click Here to Reset Password</a><!--[endif]-->
                                <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="{{ url('password/reset/'.$token) }}" style="width:142px" arcsize="7%" fillcolor="#70717D" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,12px,0px,11px"><center style="font-size:14px;line-height:24px;color:#FFFFFF;font-family:Georgia,serif;font-weight:700;mso-line-height-rule:exactly;mso-text-raise:4px">Click Here to Reset Password</center></v:textbox></v:roundrect><![endif]--></div>

                        </td>
                    </tr>
                    </tbody></table>

                <div class="column-bottom" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div>
            </td>
        </tr>
        </tbody>
    </table>
@endsection