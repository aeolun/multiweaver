@extends('emails._layout')

@section('content')
    <table class="one-col centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;table-layout: fixed;background-color: #fefefe;">
        <tbody><tr>
            <td class="column" style="padding: 0;vertical-align: top;text-align: left;">
                <div><div class="column-top" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div></div>
                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <h1 class="size-30" style="font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-size: 30px;line-height: 38px;font-family: Merriweather,Georgia,serif;color: #353638;text-align: center;">Your Player Suggestions</h1>
                            <div class="size-17" style="font-style: normal;font-weight: 400;Margin-bottom: 27px;Margin-top: 24px;font-size: 17px;line-height: 26px;font-family: Merriweather,Georgia,serif;color: #353638;text-align: center;">
                                <p>Hi {{ $user->name }},</p>

                                <p>Since you signed up on the Multiweaver website, we're sending you this email with suggestions on people to play with.</p>

                                <p>You are currently interested in playing the following games:</p>
                                <ul style="text-align:left;padding-left:100px;">
                                    @foreach($user->games()->get() as $game)
                                        <li>{{ $game->name }}</li>
                                    @endforeach
                                </ul>

                            </div>
                        </td>
                    </tr>
                    </tbody></table>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <div class="btn" style="Margin-bottom: 0;Margin-top: 0;text-align: center;">
                                <!--[if !mso]--><a style="border-radius: 3px;display: inline-block;font-size: 14px;font-weight: 700;line-height: 24px;padding: 13px 35px 12px 35px;text-align: center;text-decoration: none !important;transition: opacity 0.2s ease-in;color: #fff;font-family: Merriweather,Georgia,serif;background-color: #70717d;" href="{{ $gameUpdateLink->getUrl() }}" data-width="72" target="_blank">Update Games</a><!--[endif]-->
                                <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="{{ $gameUpdateLink->getUrl() }}" style="width:142px" arcsize="7%" fillcolor="#70717D" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,12px,0px,11px"><center style="font-size:14px;line-height:24px;color:#FFFFFF;font-family:Georgia,serif;font-weight:700;mso-line-height-rule:exactly;mso-text-raise:4px">Update Games</center></v:textbox></v:roundrect><![endif]--></div>

                        </td>
                    </tr>
                    </tbody></table>

                <div class="column-bottom" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div>
            </td>
        </tr>
        </tbody>
    </table>

    @for($i = 0; $i < count($suggestions); $i+=2)
    <table class="two-col centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;" emb-background-style="">
        <tbody><tr>
            {{ ($suggestedUser = $suggestions[$i]) && false }}
            <td class="column first" style="padding: 0;vertical-align: top;text-align: left;width: 300px;">

                <div class="image" style="font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: Merriweather,Georgia,serif;color: #353638;" align="center">
                    <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 480px;" src="{{ $suggestedUser->avatar ? url('image/300x186/fit/'.$suggestedUser->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($suggestedUser->email))).'?s=186&d=retro' }}" alt="" width="300" height="186">
                </div>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <h3 style="font-style: normal;font-weight: 700;Margin-bottom: 0;Margin-top: 24px;font-size: 18px;line-height: 24px;font-family: Merriweather,Georgia,serif;color: #353638;">
                                <strong style="font-weight: bold;"><em><a href="{{ url('users/'.$suggestedUser->id) }}">{{ $suggestedUser->name }}</a></em></strong>
                            </h3>
                            <p style="font-style: normal;font-weight: 400;Margin-bottom: 24px;Margin-top: 12px;font-size: 17px;line-height: 25px;font-family: Merriweather,Georgia,serif;color: #353638;">
                                Speaks {{ $suggestedUser->languageString() }}<br />
                                Uses {{ $suggestedUser->communicationString() }}<br />
                                From {{ \Carbon\Carbon::createFromTime($suggestedUser->gmt_time_from, 0, 0, 'GMT')->timezone($user->timezone)->format('H:i') }} to {{ \Carbon\Carbon::createFromTime($suggestedUser->gmt_time_to, 0, 0, 'GMT')->timezone($user->timezone)->format('H:i') }}<br /><br/>
                                Play together:<br />
                            <ul style="padding:0px;">
                                @foreach($suggestedUser->commonGames($user)->get() as $game)
                                    <li>{{ $game->name }}</li>
                                @endforeach
                            </ul>
                            </p>

                        </td>
                    </tr>
                    </tbody></table>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <div class="btn" style="Margin-bottom: 0;Margin-top: 0;text-align: left;">
                                <!--[if !mso]--><a style="border-radius: 3px;display: inline-block;font-size: 12px;font-weight: 700;line-height: 22px;padding: 10px 28px;text-align: center;text-decoration: none !important;transition: opacity 0.2s ease-in;color: #fff;font-family: Merriweather,Georgia,serif;background-color: #70717d;" href="{{ $suggestedUser->contactLink->getUrl() }}" data-width="68" target="_blank">Message Now</a><!--[endif]-->
                                <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="{{ $suggestedUser->contactLink->getUrl() }}" style="width:124px" arcsize="8%" fillcolor="#70717D" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Georgia,serif;font-weight:700;mso-line-height-rule:exactly;mso-text-raise:4px">Message Now</center></v:textbox></v:roundrect><![endif]--></div>

                        </td>
                    </tr>
                    </tbody></table>

                <div class="column-bottom" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div>
            </td>
            {{ ($suggestedUser = $suggestions[$i+1]) && false }}
            <td class="column second" style="padding: 0;vertical-align: top;text-align: left;width: 300px;">

                <div class="image" style="font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: Merriweather,Georgia,serif;color: #353638;" align="center">
                    <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 480px;" src="{{ $suggestedUser->avatar ? url('image/300x186/fit/'.$suggestedUser->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($suggestedUser->email))).'?s=186&d=retro' }}" alt="" width="300" height="186">
                </div>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <h3 style="font-style: normal;font-weight: 700;Margin-bottom: 0;Margin-top: 24px;font-size: 18px;line-height: 24px;font-family: Merriweather,Georgia,serif;color: #353638;">
                                <strong style="font-weight: bold;"><em><a href="{{ url('users/'.$suggestedUser->id) }}">{{ $suggestedUser->name }}</a></em></strong>
                            </h3>
                            <p style="font-style: normal;font-weight: 400;Margin-bottom: 24px;Margin-top: 12px;font-size: 17px;line-height: 25px;font-family: Merriweather,Georgia,serif;color: #353638;">
                                Speaks {{ $suggestedUser->languageString() }}<br />
                                Uses {{ $suggestedUser->communicationString() }}<br />
                                From {{ \Carbon\Carbon::createFromTime($suggestedUser->gmt_time_from, 0, 0, 'GMT')->timezone($user->timezone)->format('H:i') }} to {{ \Carbon\Carbon::createFromTime($suggestedUser->gmt_time_to, 0, 0, 'GMT')->timezone($user->timezone)->format('H:i') }}<br /><br/>
                                Play together:<br />
                            <ul style="padding:0px;">
                                @foreach($suggestedUser->commonGames($user)->get() as $game)
                                    <li>{{ $game->name }}</li>
                                @endforeach
                            </ul>
                            </p>

                        </td>
                    </tr>
                    </tbody></table>

                <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                    <tbody><tr>
                        <td class="padded" style="padding: 0;vertical-align: top;padding-left: 20px;padding-right: 20px;word-break: break-word;word-wrap: break-word;">

                            <div class="btn" style="Margin-bottom: 0;Margin-top: 0;text-align: left;">
                                <!--[if !mso]--><a style="border-radius: 3px;display: inline-block;font-size: 12px;font-weight: 700;line-height: 22px;padding: 10px 28px;text-align: center;text-decoration: none !important;transition: opacity 0.2s ease-in;color: #fff;font-family: Merriweather,Georgia,serif;background-color: #70717d;" href="{{ $suggestedUser->contactLink->getUrl() }}" data-width="68" target="_blank">Message Now</a><!--[endif]-->
                                <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="{{ $suggestedUser->contactLink->getUrl() }}" style="width:124px" arcsize="8%" fillcolor="#70717D" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Georgia,serif;font-weight:700;mso-line-height-rule:exactly;mso-text-raise:4px">Message Now</center></v:textbox></v:roundrect><![endif]--></div>

                        </td>
                    </tr>
                    </tbody></table>

                <div class="column-bottom" style="font-size: 20px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div>
            </td>
        </tr>
        </tbody></table>
    @endfor
@endsection