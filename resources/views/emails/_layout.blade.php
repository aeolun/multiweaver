<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
        .font-sans-serif {
            font-family: sans-serif;
        }
        .font-avenir {
            font-family: Avenir, sans-serif;
        }
        .mso .wrapper .font-avenir {
            font-family: sans-serif !important;
        }
        .font-lato {
            font-family: Lato, Tahoma, sans-serif;
        }
        .mso .wrapper .font-lato {
            font-family: Tahoma, sans-serif !important;
        }
        .font-cabin {
            font-family: Cabin, Avenir, sans-serif;
        }
        .mso .wrapper .font-cabin {
            font-family: sans-serif !important;
        }
        .font-open-Sans {
            font-family: "Open Sans", sans-serif;
        }
        .mso .wrapper .font-open-Sans {
            font-family: sans-serif !important;
        }
        .font-roboto {
            font-family: Roboto, Tahoma, sans-serif;
        }
        .mso .wrapper .font-roboto {
            font-family: Tahoma, sans-serif !important;
        }
        .font-ubuntu {
            font-family: Ubuntu, sans-serif;
        }
        .mso .wrapper .font-ubuntu {
            font-family: sans-serif !important;
        }
        .font-pt-sans {
            font-family: "PT Sans", "Trebuchet MS", sans-serif;
        }
        .mso .wrapper .font-pt-sans {
            font-family: "Trebuchet MS", sans-serif !important;
        }
        .font-georgia {
            font-family: Georgia, serif;
        }
        .font-merriweather {
            font-family: Merriweather, Georgia, serif;
        }
        .mso .wrapper .font-merriweather {
            font-family: Georgia, serif !important;
        }
        .font-bitter {
            font-family: Bitter, Georgia, serif;
        }
        .mso .wrapper .font-bitter {
            font-family: Georgia, serif !important;
        }
        .font-pt-serif {
            font-family: "PT Serif", Georgia, serif;
        }
        .mso .wrapper .font-pt-serif {
            font-family: Georgia, serif !important;
        }
        .font-pompiere {
            font-family: Pompiere, "Trebuchet MS", sans-serif;
        }
        .mso .wrapper .font-pompiere {
            font-family: "Trebuchet MS", sans-serif !important;
        }
        .font-roboto-slab {
            font-family: "Roboto Slab", Georgia, serif;
        }
        .mso .wrapper .font-roboto-slab {
            font-family: Georgia, serif !important;
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            .wrapper .column .size-8 {
                font-size: 8px !important;
                line-height: 14px !important;
            }
            .wrapper .column .size-9 {
                font-size: 9px !important;
                line-height: 16px !important;
            }
            .wrapper .column .size-10 {
                font-size: 10px !important;
                line-height: 18px !important;
            }
            .wrapper .column .size-11 {
                font-size: 11px !important;
                line-height: 19px !important;
            }
            .wrapper .column .size-12 {
                font-size: 12px !important;
                line-height: 19px !important;
            }
            .wrapper .column .size-13 {
                font-size: 13px !important;
                line-height: 21px !important;
            }
            .wrapper .column .size-14 {
                font-size: 14px !important;
                line-height: 21px !important;
            }
            .wrapper .column .size-15 {
                font-size: 15px !important;
                line-height: 23px !important;
            }
            .wrapper .column .size-16 {
                font-size: 16px !important;
                line-height: 24px !important;
            }
            .wrapper .column .size-17 {
                font-size: 17px !important;
                line-height: 26px !important;
            }
            .wrapper .column .size-18 {
                font-size: 17px !important;
                line-height: 26px !important;
            }
            .wrapper .column .size-20 {
                font-size: 17px !important;
                line-height: 26px !important;
            }
            .wrapper .column .size-22 {
                font-size: 18px !important;
                line-height: 26px !important;
            }
            .wrapper .column .size-24 {
                font-size: 20px !important;
                line-height: 28px !important;
            }
            .wrapper .column .size-26 {
                font-size: 22px !important;
                line-height: 31px !important;
            }
            .wrapper .column .size-28 {
                font-size: 24px !important;
                line-height: 32px !important;
            }
            .wrapper .column .size-30 {
                font-size: 26px !important;
                line-height: 34px !important;
            }
            .wrapper .column .size-32 {
                font-size: 28px !important;
                line-height: 36px !important;
            }
            .wrapper .column .size-34 {
                font-size: 30px !important;
                line-height: 38px !important;
            }
            .wrapper .column .size-36 {
                font-size: 30px !important;
                line-height: 38px !important;
            }
            .wrapper .column .size-40 {
                font-size: 32px !important;
                line-height: 40px !important;
            }
            .wrapper .column .size-44 {
                font-size: 34px !important;
                line-height: 43px !important;
            }
            .wrapper .column .size-48 {
                font-size: 36px !important;
                line-height: 43px !important;
            }
            .wrapper .column .size-56 {
                font-size: 40px !important;
                line-height: 47px !important;
            }
            .wrapper .column .size-64 {
                font-size: 44px !important;
                line-height: 50px !important;
            }
        }
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
        }
        .mso body {
            mso-line-height-rule: exactly;
        }
        .no-padding .wrapper .column .column-top,
        .no-padding .wrapper .column .column-bottom {
            font-size: 0px;
            line-height: 0px;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        td {
            padding: 0;
            vertical-align: top;
        }
        .spacer,
        .border {
            font-size: 1px;
            line-height: 1px;
        }
        .spacer {
            width: 100%;
        }
        img {
            border: 0;
            -ms-interpolation-mode: bicubic;
        }
        .image {
            font-size: 12px;
            mso-line-height-rule: at-least;
        }
        .image img {
            display: block;
        }
        .logo {
            mso-line-height-rule: at-least;
        }
        .logo img {
            display: block;
        }
        strong {
            font-weight: bold;
        }
        h1,
        h2,
        h3,
        p,
        ol,
        ul,
        blockquote,
        .image {
            font-style: normal;
            font-weight: 400;
        }
        ol,
        ul,
        li {
            padding-left: 0;
        }
        blockquote {
            Margin-left: 0;
            Margin-right: 0;
            padding-right: 0;
        }
        .column-top,
        .column-bottom {
            font-size: 20px;
            line-height: 20px;
            transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
            transition-duration: 150ms;
            transition-property: all;
        }
        .half-padding .column .column-top,
        .half-padding .column .column-bottom {
            font-size: 10px;
            line-height: 10px;
        }
        .column {
            text-align: left;
        }
        .contents {
            table-layout: fixed;
            width: 100%;
        }
        .padded {
            padding-left: 20px;
            padding-right: 20px;
            word-break: break-word;
            word-wrap: break-word;
        }
        .wrapper {
            display: table;
            table-layout: fixed;
            width: 100%;
            min-width: 620px;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .wrapper a {
            transition: opacity 0.2s ease-in;
        }
        table.wrapper {
            table-layout: fixed;
        }
        .one-col,
        .two-col,
        .three-col {
            Margin-left: auto;
            Margin-right: auto;
            width: 600px;
        }
        .centered {
            Margin-left: auto;
            Margin-right: auto;
        }
        a {
            color:#F46B34;
        }
        .btn a {
            border-radius: 3px;
            display: inline-block;
            font-size: 14px;
            font-weight: 700;
            line-height: 24px;
            padding: 13px 35px 12px 35px;
            text-align: center;
            text-decoration: none !important;
        }
        .btn a:hover {
            opacity: 0.8;
        }
        .two-col .btn a {
            font-size: 12px;
            line-height: 22px;
            padding: 10px 28px;
        }
        .three-col .btn a {
            font-size: 11px;
            line-height: 19px;
            padding: 6px 18px 5px 18px;
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            .btn a {
                display: block !important;
                font-size: 14px !important;
                line-height: 24px !important;
                padding: 13px 10px 12px 10px !important;
            }
        }
        .two-col .column {
            width: 300px;
        }
        .three-col .column {
            width: 200px;
        }
        {{ '@media only screen and (min-width: 0) {' }}
            .wrapper {
                text-rendering: optimizeLegibility;
            }
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            [class=wrapper] {
                min-width: 320px !important;
                width: 100% !important;
            }
            [class=wrapper] .one-col,
            [class=wrapper] .two-col,
            [class=wrapper] .three-col {
                width: 320px !important;
            }
            [class=wrapper] .column,
            [class=wrapper] .gutter {
                display: block;
                float: left;
                width: 320px !important;
            }
            [class=wrapper] .padded {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
            [class=wrapper] .block {
                display: block !important;
            }
            [class=wrapper] .hide {
                display: none !important;
            }
            [class=wrapper] .image img {
                height: auto !important;
                width: 100% !important;
            }
        }
        .footer {
            width: 100%;
        }
        .footer .inner {
            padding: 58px 0 29px 0;
            width: 600px;
        }
        .footer .left td,
        .footer .right td {
            font-size: 12px;
            line-height: 22px;
        }
        .footer .left td {
            text-align: left;
            width: 400px;
        }
        .footer .right td {
            max-width: 200px;
            mso-line-height-rule: at-least;
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            .footer {
                width: 320px !important;
            }
            .footer td {
                display: none;
            }
            .footer .inner,
            .footer .inner td {
                display: block;
                text-align: center !important;
                max-width: 320px !important;
                width: 320px !important;
            }
            .footer .sharing {
                Margin-bottom: 40px;
            }
            .footer .sharing div {
                display: inline-block;
            }
        }
        .wrapper h1,
        .wrapper h2,
        .wrapper h3,
        .wrapper p,
        .wrapper ol,
        .wrapper ul,
        .wrapper li,
        .wrapper blockquote,
        .image,
        .btn,
        .divider {
            Margin-bottom: 0;
            Margin-top: 0;
        }
        .wrapper .column h1 + * {
            Margin-top: 24px;
        }
        .wrapper .column h2 + * {
            Margin-top: 16px;
        }
        .wrapper .column h3 + * {
            Margin-top: 12px;
        }
        .wrapper .column p + *,
        .wrapper .column ol + *,
        .wrapper .column ul + *,
        .wrapper .column blockquote + *,
        .image + .contents td > :first-child {
            Margin-top: 27px;
        }
        .wrapper .column li + * {
            Margin-top: 14px;
        }
        .contents:nth-last-child(n+3) h1:last-child,
        .no-padding .contents:nth-last-child(n+2) h1:last-child {
            Margin-bottom: 24px;
        }
        .contents:nth-last-child(n+3) h2:last-child,
        .no-padding .contents:nth-last-child(n+2) h2:last-child {
            Margin-bottom: 16px;
        }
        .contents:nth-last-child(n+3) h3:last-child,
        .no-padding .contents:nth-last-child(n+2) h3:last-child {
            Margin-bottom: 12px;
        }
        .contents:nth-last-child(n+3) p:last-child,
        .no-padding .contents:nth-last-child(n+2) p:last-child,
        .contents:nth-last-child(n+3) ol:last-child,
        .no-padding .contents:nth-last-child(n+2) ol:last-child,
        .contents:nth-last-child(n+3) ul:last-child,
        .no-padding .contents:nth-last-child(n+2) ul:last-child,
        .contents:nth-last-child(n+3) blockquote:last-child,
        .no-padding .contents:nth-last-child(n+2) blockquote:last-child,
        .contents:nth-last-child(n+3) .image,
        .no-padding .contents:nth-last-child(n+2) .image,
        .contents:nth-last-child(n+3) .divider,
        .no-padding .contents:nth-last-child(n+2) .divider,
        .contents:nth-last-child(n+3) .btn,
        .no-padding .contents:nth-last-child(n+2) .btn {
            Margin-bottom: 27px;
        }
        .two-col .column p + *,
        .two-col .column ol + *,
        .two-col .column ul + *,
        .two-col .column blockquote + *,
        .two-col .image + .contents td > :first-child {
            Margin-top: 24px;
        }
        .two-col .column li + * {
            Margin-top: 12px;
        }
        .two-col .contents:nth-last-child(n+3) p:last-child,
        .no-padding .two-col .contents:nth-last-child(n+2) p:last-child,
        .two-col .contents:nth-last-child(n+3) ol:last-child,
        .no-padding .two-col .contents:nth-last-child(n+2) ol:last-child,
        .two-col .contents:nth-last-child(n+3) ul:last-child,
        .no-padding .two-col .contents:nth-last-child(n+2) ul:last-child,
        .two-col .contents:nth-last-child(n+3) blockquote:last-child,
        .no-padding .two-col .contents:nth-last-child(n+2) blockquote:last-child,
        .two-col .contents:nth-last-child(n+3) .image,
        .no-padding .two-col .contents:nth-last-child(n+2) .image,
        .two-col .contents:nth-last-child(n+3) .divider,
        .no-padding .two-col .contents:nth-last-child(n+2) .divider,
        .two-col .contents:nth-last-child(n+3) .btn,
        .no-padding .two-col .contents:nth-last-child(n+2) .btn {
            Margin-bottom: 24px;
        }
        .three-col .column p + *,
        .three-col .column ol + *,
        .three-col .column ul + *,
        .three-col .column blockquote + *,
        .three-col .image + .contents td > :first-child {
            Margin-top: 21px;
        }
        .three-col .column li + * {
            Margin-top: 10px;
        }
        .three-col .contents:nth-last-child(n+3) p:last-child,
        .no-padding .three-col .contents:nth-last-child(n+2) p:last-child,
        .three-col .contents:nth-last-child(n+3) ol:last-child,
        .no-padding .three-col .contents:nth-last-child(n+2) ol:last-child,
        .three-col .contents:nth-last-child(n+3) ul:last-child,
        .no-padding .three-col .contents:nth-last-child(n+2) ul:last-child,
        .three-col .contents:nth-last-child(n+3) blockquote:last-child,
        .no-padding .three-col .contents:nth-last-child(n+2) blockquote:last-child,
        .three-col .contents:nth-last-child(n+3) .image,
        .no-padding .three-col .contents:nth-last-child(n+2) .image,
        .three-col .contents:nth-last-child(n+3) .divider,
        .no-padding .three-col .contents:nth-last-child(n+2) .divider,
        .three-col .contents:nth-last-child(n+3) .btn,
        .no-padding .three-col .contents:nth-last-child(n+2) .btn {
            Margin-bottom: 21px;
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            .wrapper p + *,
            .wrapper ol + *,
            .wrapper ul + *,
            .wrapper blockquote + *,
            .image + .contents td > :first-child {
                Margin-top: 27px !important;
            }
            .contents:nth-last-child(n+3) p:last-child,
            .no-padding .contents:nth-last-child(n+2) p:last-child,
            .contents:nth-last-child(n+3) ol:last-child,
            .no-padding .contents:nth-last-child(n+2) ol:last-child,
            .contents:nth-last-child(n+3) ul:last-child,
            .no-padding .contents:nth-last-child(n+2) ul:last-child,
            .contents:nth-last-child(n+3) blockquote:last-child,
            .no-padding .contents:nth-last-child(n+2) blockquote:last-child,
            .contents:nth-last-child(n+3) .image:last-child,
            .no-padding .contents:nth-last-child(n+2) .image:last-child,
            .contents:nth-last-child(n+3) .divider:last-child,
            .no-padding .contents:nth-last-child(n+2) .divider:last-child,
            .contents:nth-last-child(n+3) .btn:last-child,
            .no-padding .contents:nth-last-child(n+2) .btn:last-child {
                Margin-bottom: 27px !important;
            }
            .column li + * {
                Margin-top: 14px !important;
            }
        }
        .divider {
            font-size: 4px;
            line-height: 4px;
        }
        .contents .divider {
            Margin-bottom: 27px;
        }
        .divider .bullet {
            border-radius: 2px;
            display: inline-block;
            font-size: 4px;
            height: 4px;
            line-height: 4px;
            width: 4px;
        }
        .mso .bullet {
            display: none;
        }
        .one-col,
        two-col,
        .three-col {
            table-layout: fixed;
        }
        table.spacer {
            height: 54px;
        }
        .wrapper a {
            text-decoration: underline;
        }
        .wrapper h1 {
            font-size: 32px;
            line-height: 40px;
        }
        .wrapper h2 {
            font-size: 22px;
            line-height: 30px;
        }
        .wrapper h3 {
            font-size: 18px;
            line-height: 24px;
        }
        .wrapper h1 a,
        .wrapper h2 a,
        .wrapper h3 a {
            text-decoration: none;
        }
        .wrapper p,
        .wrapper ol,
        .wrapper ul {
            font-size: 17px;
            line-height: 25px;
        }
        .wrapper ol {
            Margin-left: 24px;
        }
        .wrapper ol li {
            padding-left: 4px;
        }
        .wrapper ul {
            Margin-left: 18px;
        }
        .wrapper ul li {
            padding-left: 9px;
        }
        .wrapper blockquote {
            Margin-left: 0;
            padding-left: 17px;
        }
        .two-col ol {
            Margin-left: 21px;
        }
        .two-col ol li {
            padding-left: 3px;
        }
        .two-col ul {
            Margin-left: 18px;
        }
        .two-col ul li {
            padding-left: 6px;
        }
        .two-col blockquote {
            border-left-width: 3px;
            padding-left: 15px;
        }
        .three-col ul {
            Margin-left: 16px;
        }
        .three-col ul li {
            padding-left: 6px;
        }
        .three-col ol {
            Margin-left: 18px;
        }
        .three-col ol li {
            padding-left: 4px;
        }
        .three-col blockquote {
            border-left-width: 2px;
            padding-left: 13px;
        }
        .wrapper h2 {
            font-weight: 700;
        }
        .wrapper h3 {
            font-weight: 700;
        }
        .wrapper blockquote {
            font-style: italic;
        }
        .header {
            Margin-left: auto;
            Margin-right: auto;
            width: 560px;
        }
        .preheader table {
            width: 560px;
        }
        .preheader .title,
        .preheader .webversion {
            padding-bottom: 12px;
            font-size: 12px;
            line-height: 21px;
        }
        .preheader .title {
            text-align: left;
        }
        .preheader .webversion {
            text-align: right;
            width: 300px;
        }
        .header td {
            font-size: 24px;
        }
        .header .logo div {
            font-weight: bold;
        }
        .header .logo div a {
            text-decoration: none;
        }
        .header .logo div.logo-center {
            text-align: center;
        }
        .header .logo div.logo-center img {
            Margin-left: auto;
            Margin-right: auto;
        }
        {{ '@media only screen and (max-width: 620px) {' }}
            [class=wrapper] table.spacer {
                height: 28px !important;
            }
            [class=wrapper] .header {
                width: 280px !important;
            }
            [class=wrapper] .webversion {
                display: none;
            }
            [class=wrapper] .title {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
            [class=wrapper] .preheader table {
                width: 320px !important;
            }
            [class=wrapper] .logo img {
                max-width: 280px !important;
                height: auto !important;
            }
            [class=wrapper] blockquote {
                border-left-width: 4px !important;
                margin-left: 0 !important;
                padding-left: 14px !important;
            }
            [class=wrapper] h1 {
                font-size: 32px !important;
                line-height: 42px !important;
            }
            [class=wrapper] h2 {
                font-size: 22px !important;
                line-height: 30px !important;
            }
            [class=wrapper] h3 {
                font-size: 18px !important;
                line-height: 26px !important;
            }
            [class=wrapper] .one-col p,
            [class=wrapper] .two-col p,
            [class=wrapper] .three-col p,
            [class=wrapper] .one-col ol,
            [class=wrapper] .two-col ol,
            [class=wrapper] .three-col ol,
            [class=wrapper] .one-col ul,
            [class=wrapper] .two-col ul,
            [class=wrapper] .three-col ul {
                font-size: 17px !important;
                line-height: 27px !important;
            }
            [class=wrapper] ol {
                margin-left: 24px !important;
            }
            [class=wrapper] ol li {
                padding-left: 4px !important;
            }
            [class=wrapper] ul {
                margin-left: 19px !important;
            }
            [class=wrapper] ul li {
                padding-left: 9px !important;
            }
            [class=wrapper] .second .column-top,
            [class=wrapper] .third .column-top {
                display: none;
            }
            [class=wrapper] .show {
                display: block !important;
                font-size: 1px;
                line-height: 1px;
            }
            [class=wrapper] .hide {
                display: none !important;
            }
        }
    </style>
    <!--[if !mso]><!--><style type="text/css">
        {{ '@import url(https://fonts.googleapis.com/css?family=Merriweather:400italic,400,700,700italic);' }}
    </style><link href="https://fonts.googleapis.com/css?family=Merriweather:400italic,400,700,700italic" rel="stylesheet" type="text/css"><!--<![endif]--><style type="text/css">
        .wrapper h1{}.wrapper h1{font-family:Merriweather,Georgia,serif}.mso .wrapper h1{font-family:Georgia,serif !important}.wrapper h2{}.wrapper h2{font-family:Merriweather,Georgia,serif}.mso .wrapper h2{font-family:Georgia,serif !important}.wrapper h3{}.wrapper h3{font-family:Merriweather,Georgia,serif}.mso .wrapper h3{font-family:Georgia,serif !important}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{font-family:Merriweather,Georgia,serif}.mso .wrapper p,.mso .wrapper ol,.mso .wrapper ul,.mso .wrapper .image{font-family:Georgia,serif !important}.wrapper .btn a{}.wrapper .btn a{font-family:Merriweather,Georgia,serif}.mso .wrapper .btn a{font-family:Georgia,serif !important}.logo div{}.logo div{font-family:Roboto,Tahoma,sans-serif}.mso .logo div{font-family:Tahoma,sans-serif
        !important}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{font-family:Merriweather,Georgia,serif}.mso .title,.mso .webversion,.mso .fblike,.mso .tweet,.mso .linkedinshare,.mso .forwardtoafriend,.mso .link,.mso .address,.mso .permission,.mso .campaign{font-family:Georgia,serif !important}body,.wrapper,.emb-editor-canvas{background-color:#fefefe}blockquote{border-left:4px solid #cbcbcb}.wrapper h1{color:#353638}.wrapper h2{color:#353638}.wrapper h3{color:#353638}.wrapper p,.wrapper ol,.wrapper ul{color:#353638}.wrapper .image{color:#353638}.wrapper a{color:#353638}.wrapper a:hover{color:#1c1d1e !important}.wrapper .btn a{background-color:#70717d;color:#fff}.wrapper .btn a:hover{color:#fff !important}.logo div{color:#c3ced9}.logo
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     div a{color:#c3ced9}.logo div a:hover{color:#c3ced9 !important}.divider .bullet{background-color:#cbcbcb}.bullet-light{display:none}.title,.webversion,.header,.footer .inner td{color:#a3a4ad}.wrapper .preheader a,.wrapper .header a,.wrapper .footer a{color:#a3a4ad}.wrapper .preheader a:hover,.wrapper .header a:hover,.wrapper .footer a:hover{color:#7b7c89 !important}.wrapper .footer .fblike,.wrapper .footer .tweet,.wrapper .footer .linkedinshare,.wrapper .footer .forwardtoafriend{background-color:#7f7f7f}
    </style>
</head>
<!--[if mso]>
<body class="mso">
<![endif]-->
<!--[if !mso]><!-->
<body class="ff-spacing full-padding" style="margin: 0;padding: 0;min-width: 100%;background-color: #fefefe;">
<!--<![endif]-->
<center class="wrapper" style="display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #fefefe;">
    <table class="spacer" style="border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;width: 100%;height: 54px;"><tbody><tr><td style="padding: 0;vertical-align: top;">&nbsp;</td></tr></tbody></table>
    <table class="preheader centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;">
        <tbody><tr>
            <td style="padding: 0;vertical-align: top;">
                <table style="border-collapse: collapse;border-spacing: 0;width: 560px;">
                    <tbody><tr>
                        <td class="title" style="padding: 0;vertical-align: top;font-family: Merriweather,Georgia,serif;color: #a3a4ad;padding-bottom: 12px;font-size: 12px;line-height: 21px;text-align: left;">

                        </td>
                        <td class="webversion" style="padding: 0;vertical-align: top;font-family: Merriweather,Georgia,serif;color: #a3a4ad;padding-bottom: 12px;font-size: 12px;line-height: 21px;text-align: right;width: 300px;">

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    <table class="header centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;">
        <tbody><tr>
            <td style="padding: 0;vertical-align: top;">

                <table class="logo" style="border-collapse: collapse;border-spacing: 0;width: 560px;">
                    <tbody><tr>
                        <div class="logo-center"><img src="https://multiweaver.com/images/newlogo.png" height="120" width="120" /></div>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>


    @yield('content')

    <table class="footer centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;">
        <tbody><tr>
            <td style="padding: 0;vertical-align: top;">&nbsp;</td>
            <td class="inner" style="padding: 58px 0 29px 0;vertical-align: top;width: 600px;">
                <table class="centered" style="border-collapse: collapse;border-spacing: 0;" align="center">
                    <tbody><tr>
                        <td style="padding: 0;vertical-align: top;color: #a3a4ad;font-size: 12px;line-height: 22px;mso-line-height-rule: at-least;">
                            Thank you for using Multiweaver. If you have any questions or comments, always feel free to email me at <a href="mailto:contact@multiweaver.com">contact@multiweaver.com</a>.
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="centered" style="border-collapse: collapse;border-spacing: 0;" align="center">
                    <tbody><tr>
                        <td style="padding: 0;vertical-align: top;color: #a3a4ad;font-size: 12px;line-height: 22px;max-width: 200px;mso-line-height-rule: at-least;">
                            <div class="sharing">



                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td style="padding: 0;vertical-align: top;">&nbsp;</td>
        </tr>
        </tbody></table>
</center>