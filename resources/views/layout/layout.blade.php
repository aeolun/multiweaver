<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="shortcut icon" href="images/newlogo.png">

    <title>@yield('title', 'Multiweaver: Connecting the worlds Gamers!')</title>
    <meta name="description" content="Find people to play games you are interested with, all over the world.">
    <meta name="_token" content="{{ csrf_token() }}" >

    <link href="//fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ url('css/all.css') }}">
    <link rel="stylesheet" href="{{ url('css/app.css') }}">

    @yield('head')
</head>
<body>
<div class="container">
    <nav class="navbar navbar-fixed-top navbar-dark">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img alt="Brand" src="{{ asset('images/newlogo.png') }}" height="28" style="display:inline-block;" />
            Multiweaver
        </a>

        <ul class="nav navbar-nav">
            <li class="nav-item {{ Route::getCurrentRoute()->getPath() == 'users' ? 'active' : '' }}"><a class="nav-link" href="{{ url('users') }}"><i class="fa fa-search"></i> Search</a></li>
            <li class="nav-item {{ Route::getCurrentRoute()->getPath() == 'games' ? 'active' : '' }}"><a class="nav-link" href="{{ url('games') }}"><i class="fa fa-list"></i> Games</a></li>
        </ul>
        <ul class="nav navbar-nav pull-right">
            @if(Auth::check())
                <li class="nav-item {{ Route::getCurrentRoute()->getPath() == ('users/{users}/edit') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('users/'.Auth::user()->id.'/edit') }}"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>
                </li>
                <li class="nav-item {{ Route::getCurrentRoute()->getPath() == ('users/{users}') && $user->id == Auth::user()->id ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('users/'.Auth::user()->id) }}"><i class="fa fa-gamepad"></i> Your Games</a>
                </li>
                <li class="nav-item {{ Route::getCurrentRoute()->getPath() == ('conversations') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('conversations') }}"><i class="fa fa-comment"></i> Conversations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                </li>
            @else
                <li class="nav-item"><a class="nav-link" href="{{ url('auth/login') }}"><i class="fa fa-sign-in"></i> Log In</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('auth/register') }}"><i class="fa fa-user-plus"></i> Sign Up</a></li>
            @endif
        </ul>
    </nav>
    @if(Auth::check())
        @if((Auth::user()->play_time_from == 0 && Auth::user()->play_time_to == 0) || Auth::user()->languages->count() == 0)
            <div class="alert alert-warning row-margin" role="alert">
                Your play time or languages are not set! <a href="{{ url('users/'.Auth::user()->id.'/edit') }}">Please update your profile</a>, <strong>we can't match you to other players unless they're set</strong>.
            </div>
        @elseif(Auth::user()->games->count() == 0)
            <div class="alert alert-info row-margin" role="alert">
                <strong>You are not interested in any games!</strong> <a href="{{ url('games') }}">Choose a few games you want to play</a> so we can match you! You won't be shown in the user
                list unless you have games selected.
            </div>
        @endif
        @if(Auth::user()->steam_id && strpos(Auth::user()->email, '@') === false)
            <div class="help-block alert alert-danger row-margin">Since you signed in through steam, please <a href="{{url('users/'.Auth::user()->id.'/edit')}}">update your email address</a> or we won't be able to send you update emails.</div>
        @endif
    @endif
    @if(Session::get('status'))
        <div class="alert alert-success alert-status row-margin">
            {{ Session::get('status') }}
        </div>
    @endif
    <div class="content">
        @yield('content')
    </div>
    <footer class="text-xs-center row-margin text-muted">
        Created by an army of Dwarves and a <a href="http://www.amazon.com/gp/product/076536543X/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=076536543X&linkCode=as2&tag=multiweaver-20&linkId=MCAEH6BAHRAW4XW3" target="_blank" rel="nofollow">Mistborn Princess</a>. All Rights Reserved.
    </footer>
</div>
<script src="{{ url('js/all.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
</script>

    @yield('script')

@if(App::environment() == 'production')
    <script>
        !function(g,s,q,r,d){r=g[r]=g[r]||function(){(r.q=r.q||[]).push(
                        arguments)};d=s.createElement(q);q=s.getElementsByTagName(q)[0];
            d.src='//d1l6p2sc9645hc.cloudfront.net/tracker.js';q.parentNode.
            insertBefore(d,q)}(window,document,'script','_gs');

        _gs('GSN-873019-F');
    </script>

@endif
<script>
    (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
        t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
    })(window, document, '_gscq','script','//widgets.getsitecontrol.com/43619/script.js');
</script>
@if(Auth::check())
    <script>
        _gscq.push(['user','name', '{{ Auth::user()->name }}']);
        _gscq.push(['user','email', '{{ Auth::user()->email }}']);
    </script>
@endif


</body>
</html>
