<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    {{ $user->name }} Contacted | Multiweaver
@endsection

@section('content')
    <h1>We sent a message</h1>
    <p>Please wait for your target to reply.</p>
    <p><a href="{{ url('users') }}" class="btn btn-primary">Find more people to play with</a></p>
@endsection