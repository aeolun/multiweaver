<!-- resources/views/auth/register.blade.php -->
@extends('layout.layout')

@section('title')
    {{ $user->name }} Profile | Multiweaver
@endsection

@section('content')
    <form class="signup-form form-horizontal glass-panel" enctype="multipart/form-data" method="POST" action="{{ url('users/'.$user->id) }}">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT">
        <h2>Login Info</h2>
        <div class="form-group row">
            <label class="col-sm-4 control-label">Name <span class="text-info">(public)</span></label>
            <div class="col-sm-8"><input class="form-control" type="text" name="name" value="{{ $user->name }}"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
                <input class="form-control" style="{{ $user->steam_id && strpos(Auth::user()->email, '@') === false ? 'border-color:#a94442;' : '' }}" type="email" name="email" value="{{ $user->email }}">
            </div>

        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8"><input class="form-control" type="password" placeholder="{{ $user->password ? 'Enter if you want to change the password' : 'Enter here to set a password for your account' }}" name="password"></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Confirm Password</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" placeholder="Confirm the password change" name="password_confirmation">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Avatar <span class="text-info">(public)</span></label>
            <div class="col-sm-8">
                <img src="{{ url('image/100x100/fit/'.$user->avatar) }}" class="img-thumbnail" height="100" /><br />
                <input class="form-control row-margin" type="file" name="avatar">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Birthdate</label>
            <div class="col-sm-8">
                <input class="form-control" type="date" name="birthdate" value="{{ date('Y-m-d', strtotime($user->birthdate)) }}">
            </div>
        </div>

            <div class="form-group row">
                <label class="col-sm-4 control-label">About Me <span class="text-info">(public)</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="about" rows="4">{{ $user->about }}</textarea>
                </div>
            </div>

        <h2>Game Preferences</h2>
            <p class="text-info">All game preferences are public</p>
        <div class="form-group row">
            <label class="col-sm-4 control-label">Languages</label>
            <div class="col-sm-8">
                <div class="row">
                @foreach($languages as $language)
                    <div class="col-xs-6 col-lg-4">
                    <div class="checkbox">
                        <label><input type="checkbox" {{ Auth::user()->languages->contains($language) ? 'checked="checked"' : '' }} value="{{ $language->id }}" name="language[]" /> <img class="icon" src="{{ url('images/flags/'.$language->icon.'.png') }}" height="16" width="16" />  {{ $language->name ? $language->name : $language->english_name }}</label>
                    </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Timezone</label>
            <div class="col-sm-8">
                <select name="timezone" class="form-control">
                    @foreach($timezones as $key => $zone)
                        <option {{ $key == $user->timezone ? 'selected="selected"' : '' }} value="{{ $key }}">{{ $zone }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-xs-12 col-lg-4 control-label">
                Available Time
                <div class="help-block">
                    This is the time when you are generally available to play games. This is used to match you to people that are online at the same time as you.
                </div>
            </label>
            <div class="col-xs-12 col-lg-8">
                <div class="row available-time hidden-xl-down">
                    <div class="col-xs-12">
                        <b class="col-md-1">00:00</b> <input id="available-time" style="width:55%;" name="play_time" type="text" value="" data-slider-min="0" data-slider-max="24" data-slider-step="1" data-slider-value="[{{ $user->play_time_from }},{{ $user->play_time_to }}]"/> <b class="col-md-1">23:59</b>
                    </div>
                </div>
                <noscript>
                    <div class="row">
                        <div class="col-sm-5">
                            <select class="form-control" name="play_time_from">
                                @for($i = 0; $i < 24; $i++)
                                    <option value="{{ $i }}" {{ $user->play_time_from == $i ? 'selected="selected"' : '' }}>{{ $i }}:00</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-2">
                            to
                        </div>
                        <div class="col-sm-5">
                            <select class="form-control" name="play_time_to">
                                @for($i = 1; $i <= 24; $i++)
                                    <option value="{{ $i }}" {{ $user->play_time_to == $i ? 'selected="selected"' : '' }}>{{ $i }}:00</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </noscript>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Communication Methods</label>
            <div class="col-sm-8">
                <div class="row">
                @foreach($communication_methods as $method)
                    <div class="col-xs-6 col-lg-4">
                        <div class="checkbox">
                            <label><input type="checkbox" {{ Auth::user()->communication_methods->contains($method) ? 'checked="checked"' : '' }} value="{{ $method->id }}" name="method[]" /> <img class="icon" src="{{ url('images/communication/'.$method->icon.'.png') }}" height="16" width="16" />  {{ $method->name }}</label>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 control-label">Looking For</label>
            <div class="col-sm-8">
                <select name="looking_for" class="form-control">
                    @foreach($looking_for as $zone=>$display)
                        <option {{ $zone == $user->looking_for ? 'selected="selected"' : '' }} value="{{ $zone }}">{{ $display }}</option>
                    @endforeach
                </select>
            </div>
        </div>

            <div class="form-group row">
                <label class="col-sm-4 control-label">Maturity</label>
                <div class="col-sm-8">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="maturity" value="1" {{ $user->maturity == 1 ? 'checked="checked"' : '' }} /> Look for 'adult' gamers
                        </label>
                    </div>
                </div>
            </div>

        <div class="form-group row">
            <div class="col-sm-4">&nbsp;</div>
            <div class="col-sm-8">
                <button class="btn btn-primary" type="submit">Update</button>
            </div>
        </div>
    </form>
@endsection