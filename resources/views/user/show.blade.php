<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    {{ $user->name }} | Multiweaver
@endsection

@section('content')
    @if(Auth::user() != $user)
        <div class="row">
            <div class="col-md-12">
                <div class="glass-panel">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-fluid pull-left profile-image" src="{{ $user->avatar ? url('image/180x180/fit/'.$user->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($user->email))).'?s=180&d=retro' }}" alt="{{ $user->name }}">
                        @if(Auth::user())
                            <a class="btn btn-lg btn-primary pull-right" href="{{ url('conversations?users='.$user->id.'#new') }}">Contact</a>
                        @else
                            <a class="btn btn-lg btn-primary pull-right" href="{{ url('auth/register') }}">Sign up to contact this user</a>
                        @endif
                        <h1>{{ $user->name }}</h1>
                        Signed up {{ $user->created_at->format('Y-m-d') }}<br />
                        Speaks {{ $user->languageString() }}<br />
                        Uses {{ $user->communicationString() }}<br />
                        Interested in playing {{ $user->games()->count() }} games<br />
                        @if(Auth::check())
                            From {{ \Carbon\Carbon::createFromTime($user->gmt_time_from, 0, 0, 'GMT')->timezone(Auth::user()->timezone)->format('H:i') }} to {{ \Carbon\Carbon::createFromTime($user->gmt_time_to, 0, 0, 'GMT')->timezone(Auth::user()->timezone)->format('H:i') }} <span class="text-info">(your time)</span>
                        @else
                            From {{ $user->gmt_time_from }}:00 to {{ $user->gmt_time_to }}:00 GMT
                        @endif
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endif
    @if($user->about)
        <h2 class="row-margin">About Me</h2>
        <div class="glass-panel">
            <div>{!! Markdown::convertToHtml($user->about) !!}</div>
        </div>
    @endif

    <div class="row row-margin">
        <div class="col-md-12">
            <h2 class="">Interested in Playing</h2>
        </div>
        @foreach($user->games as $game)
            @include('_partials.game', ['user'=>$user])
        @endforeach
    </div>
@endsection