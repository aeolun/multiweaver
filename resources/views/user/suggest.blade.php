<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    Suggestions for {{ Auth::user()->name }} | Multiweaver
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row masonry">
                @foreach($users as $user)
                    @include('_partials.user', ['size'=>3, 'show' => 'common'])
                @endforeach
            </div>
            <div class="text-center">
                @include('pagination.default', ['paginator' => $users])
            </div>
        </div>
    </div>

@endsection