<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    Contact {{ $user->name }} | Multiweaver
@endsection

@section('content')
    <h1>Ask {{ $user->name }} to play</h1>
    <p>This will send a short message to the user. If they are online on the website, if not, on their email. You can add your own personal note (if you want) to make them more likely to reply.</p>
    <p>You are both interested in the following games:</p>
    <ul>
        <?php $int = false ?>
        @foreach(Auth::user()->games as $game)
            @if($user->isInterested($game))
                <li>{{ $game->name }}</li>
                <?php $int = true ?>
            @endif
        @endforeach
        @if(!$int)
            <li>No games</li>
        @endif
    </ul>
    <form action="{{ url('users/'.$user->id.'/send') }}" method="post">
        {!! csrf_field() !!}
        <textarea name="message" placeholder="Some personal note" class="form-control"></textarea>
        <input type="submit" class="btn btn-primary row-margin" value="Send Message" />
    </form>
@endsection