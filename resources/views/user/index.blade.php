<!-- resources/views/auth/login.blade.php -->
@extends('layout.layout')

@section('title')
    Users ({{ $users->currentPage() }}) | Multiweaver
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-4 col-xs-12 row-margin">
            <form action="{{ url('users') }}" method="get" class="glass-panel">
                <h2>Search</h2>
                <input class="form-control" type="text" placeholder="Game Name" name="game" value="{{ $filter['game'] or '' }}" />
                @if(Auth::check())
                    <div class="row-margin"><strong>My Games</strong></div>
                    <div class="row-margin">
                        <label>
                            <input type="checkbox" name="my_games" value="1"  {{ (isset($filter['my_games']) && $filter['my_games'] == true) ? 'checked="checked"' : '' }}  /> Show only users with games I'm interested in
                        </label>
                    </div>
                @endif
                <div class="row-margin"><strong>Looking For</strong></div>
                <select name="looking_for" class="form-control row-margin">
                    <?php $options = ['casual', 'competitive'] ?>
                    @foreach($options as $key)
                        <option {{ (isset($filter['looking_for']) && $filter['looking_for'] == $key) ? 'selected="selected"' : '' }} value="{{ $key }}">{{ $key }}</option>
                    @endforeach
                </select>
                <div class="row-margin"><strong>Timezone</strong></div>
                <select name="timezone_offset" class="form-control row-margin">
                    {{ $default = isset($filter['timezone_offset']) ? $filter['timezone_offset'] : null }}
                    @for($i = -12; $i <= 12; $i++)
                        <option {{ $i == $default ? 'selected="selected"' : '' }} value="{{ $i }}">{{ $i > 0 ? '+' : '' }}{{ $i == 0 ? 'GMT' : $i.':00' }}</option>
                    @endfor
                </select>
                <div class="row row-margin">
                    <div class="col-md-6 col-lg-5">
                        <select class="form-control" name="play_time_from">
                            @for($i = 0; $i < 24; $i++)
                                <option value="{{ $i }}" {{ (isset($filter['play_time_from']) && $filter['play_time_from'] == $i) ? 'selected="selected"' : '' }}>{{ $i }}:00</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-sm-2 hidden-md-down">
                        to
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <select class="form-control" name="play_time_to">
                            @for($i = 1; $i <= 24; $i++)
                                <option value="{{ $i }}" {{ (isset($filter['play_time_to']) && $filter['play_time_to'] == $i) ? 'selected="selected"' : '' }}>{{ $i }}:00</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="row-margin"><strong>Communication</strong></div>
                <div class="row row-margin">
                    <div class="col-md-12">
                        <select name="communication[]" data-style="btn-secondary" class="selectpicker" multiple data-selected-text-format="count > 3" data-tick-icon="fa fa-check">
                            @foreach($communication_methods as $method)
                                <option class="dropdown-item" {{ in_array($method->id, $filter['communication']) ? 'selected="selected"' : '' }} value="{{ $method->id }}" data-content="<img class='icon' src='{{ url('images/communication/'.$method->icon.'.png') }}' height='16' width='16' /> {{ $method->name }}" />
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row-margin"><strong>Language</strong></div>
                <div class="row row-margin">
                    <div class="col-md-12">
                        <select name="language[]" data-style="btn-secondary" class="selectpicker" multiple data-selected-text-format="count > 3" data-tick-icon="fa fa-check">
                            @foreach($languages as $language)
                                <option class="dropdown-item" {{ in_array($language->id, $filter['language']) ? 'selected="selected"' : '' }} value="{{ $language->id }}" data-content="<img class='icon' src='{{ url('images/flags/'.$language->icon.'.png') }}' height='16' width='16' /> {{ $language->name ? $language->name : $language->english_name }}" />
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row-margin"><strong>Maturity</strong></div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="mature" value="1" {{ isset($filter['mature']) && $filter['mature'] ? 'checked="checked"' : '' }} /> Look for 'adult' gamers
                    </label>
                </div>
                <input type="submit" value="Filter" class="btn btn-primary row-margin" />
            </form>
        </div>
        <div class="col-lg-9 col-md-8 row-margin">
            <div class="row masonry">
                @foreach($users as $user)
                    @include('_partials.user', ['size'=>4])
                @endforeach
            </div>
            <div class="text-center">
                @include('pagination.default', ['paginator' => $users])
            </div>
        </div>
    </div>

@endsection