<div class="col-lg-{{ $size or 3 }} col-md-6 col-xs-12">
    <div class="card">
        <div class="card-img-top-container">

            <?php $avatar = url('image/238x152/fit/'.($user->avatar?$user->avatar:"5200.png")) ?>

            <img class="card-img-top img-fluid" src="{{ $avatar }}" alt="{{ $user->name }}">
        </div>
        <div class="card-block">

            <h4 class="card-title">
                @if(Auth::check())
                    <a href="{{ url('conversations?users='.$user->id.'#new') }}" class="btn btn-sm btn-primary-outline pull-right"><i class="fa fa-comment"></i></a>
                @endif
                <a href="{{ url('users/'.$user->id) }}">
                    {{ $user->name }}
                </a>
            </h4>

            <div class="card-text row-margin">
                <div class="row communication-bar">
                    <div class="col-md-5">
                        @foreach($user->languages as $language)
                            <img class="icon" data-toggle="tooltip" title="{{ $language->name }}" src="{{ url('images/flags/'.$language->icon.'.png') }}" height="16" width="16" />
                        @endforeach
                    </div>
                    <div class="col-md-7 text-xs-right">
                        @foreach($user->communication_methods as $method)
                            <img class="icon" data-toggle="tooltip" title="{{ $method->name }}" src="{{ url('images/communication/'.$method->icon.'.png') }}" height="16" width="16" />
                        @endforeach
                    </div>
                </div>


                @if(isset($show) && $show == 'common' && Auth::check())
                    <div class="common-bar">
                        @foreach($user->commonGames(Auth::user())->get()->slice(0,3) as $common)
                            <img src="{{ url('image/68x40/fit/'.$common->logo_url) }}" data-toggle="tooltip" title="{{ $common->name }}" />
                        @endforeach
                        <br />
                        {{! $count = $user->commonGames(Auth::user())->count() }}
                        @if($count > 3)
                            <a data-toggle="tooltip" title="{{ implode(', ', array_slice($user->commonGames(Auth::user())->lists('name')->toArray(), 3)) }}">{{ $games = $count-3 }} other {{ $games > 1 ? 'games' : 'game' }} in common</a><br />
                        @endif
                    </div>
                @else
                    <div class="interested-bar">
                        <a data-toggle="tooltip" title="{{ implode(', ', $user->games()->lists('name')->toArray()) }}">Interested in {{ $user->games()->count() }} games</a><br />
                    </div>
                @endif
                <div class="play-time-bar">
                    @if(Auth::check())
                        From {{ \Carbon\Carbon::createFromTime($user->gmt_time_from, 0, 0, 'GMT')->timezone(Auth::user()->timezone)->format('H:i') }} to {{ \Carbon\Carbon::createFromTime($user->gmt_time_to, 0, 0, 'GMT')->timezone(Auth::user()->timezone)->format('H:i') }}
                    @else
                        From {{ $user->gmt_time_from }}:00 to {{ $user->gmt_time_to }}:00 GMT
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>