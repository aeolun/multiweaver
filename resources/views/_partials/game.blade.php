<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <div class="card" title="{{ $game->name }}">
        <div class="img-game card-img-top-container">
            <img class="card-img-top img-fluid" src="{{ url('image/460x215/fit/'.$game->logo_url) }}" alt="{{ $game->name }}">
        </div>
        <div class="card-block">
            <a href="{{ url('games/'.$game->id) }}"><h4 class="card-title title-game">{{ $game->name }} {{ isset($user) ? $user->getPlatform($game) : '' }}</h4></a>
            <p class="card-text">{{ $game->users()->count() }} players interested</p>
            @if(Auth::user())
                <div class="btn-group">
                    @if(Auth::user()->isInterested($game))
                        <a href="{{ url('games/play/'.$game->id) }}" class="btn btn-success"><i class="fa fa-remove"></i> Interested
                            {{ Auth::user()->getPlatform($game) }}</a>
                    @else
                        <a href="{{ url('games/play/'.$game->id) }}" class="btn btn-success-outline">Interested</a>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>