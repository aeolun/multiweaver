@extends('layout.layout')

@section('content')
    <div class="glass-panel">
        <div id="carousel-front" class="carousel slide" data-ride="carousel" data-pause="hover">
            <ol class="carousel-indicators">
                @foreach($carousel as $idx => $game)
                <li data-target="#carousel-front" data-slide-to="{{ $idx }}" class="{{ $idx == 0 ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach($carousel as $idx => $game)
                <div class="carousel-item {{ $idx == 0 ? 'active' : '' }}">
                    <a href="{{ url('games/'.$game->id) }}">
                        <img src="{{ url('image/1110x362/fit/'.$game->banner_url) }}" alt="{{ $game->name }}">
                        <div class="carousel-caption">
                            <h3>{{ $game->name }}</h3>
                            <p>{{ $game->users()->count() }} players interested in {{ $game->name }}. Click here to join them!</p>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <a class="left carousel-control" href="#carousel-front" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-front" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="text-xs-center row-margin">Want your game to appear here? Go to the game page and add a banner for the game!</div>
    </div>
    <div class="glass-panel row-margin">
    <div class="row" style="margin:50px 0px;">
        <div class="col-sm-4">
            <div class="text-xs-center main-icon">
                <i class="fa fa-sign-in fa-4x text-primary"></i>
            </div>
            <h2 class="row-margin">1. Sign Up</h2>
            <p>We only require an email (to contact you), name (to call you by), and a password. Anything else is optional.</p>
        </div>
        <div class="col-sm-4">
            <div class="text-xs-center main-icon">
                <i class="fa fa-list fa-4x text-primary"></i>
            </div>
            <h2 class="row-margin">2. Select Games</h2>
            <p>Select the games you are interested in playing with other people (be it co-op, versus or otherwise).</p>
        </div>
        <div class="col-sm-4">
            <div class="text-xs-center main-icon">
                <i class="fa fa-search fa-4x text-primary"></i>
            </div>
            <h2 class="row-margin">3. Find Players</h2>
            <p>You can search on the website for people to play with, but <strong>we'll also send you an email once in a while to tell you about new users interested in the same games as you</strong>.</p>
        </div>
    </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-12">
            <div class="jumbotron bg-glass">
                <h1>Welcome to Multiweaver</h1>
                <p class="lead">This site was created to help you find new people to play (online) games with. Since I had issues finding people to play with after I moved to the other side of the world, I figured other people might have the same problem, and this is the result!</p>
                <h2 class="text-center">Get Started Now!</h2>
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-primary btn-block btn-lg" href="{{ url('users') }}" role="button">Find New People to Play With!</a>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-secondary btn-block btn-lg" href="{{ url('games') }}" role="button">Select Games You Want To Play!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
