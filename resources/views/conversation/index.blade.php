@extends('layout.layout')


@section('title')
    Conversations | Multiweaver
@endsection

@section('content')
    <div class="container-fluid">
        <div class="conversation-box bg-glass row">
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked row-margin">
                    <li class="nav-item">
                        <a class="nav-link active new-conversation-link" href="#new"><strong>New Conversation</strong></a>
                    </li>
                </ul>
                <ul class="nav nav-pills nav-stacked conversation-list">

                    @foreach($conversations as $conversation)
                        <li class="nav-item">
                            <a class="nav-link conversation-link" id="conversation-{{ $conversation->id }}" data-conversation-id="{{ $conversation->id }}" href="#{{ $conversation->id }}">{{ $conversation->title }}</a>
                        </li>
                    @endforeach
                </ul>

            </div>
            <div class="col-md-9">
                <div class="new-conversation">
                    <h2>Create new conversation</h2>
                    <form class="create-conversation-form">
                        <input type="text" name="title" autofocus="autofocus" placeholder="title" class="form-control" />
                        <div class="row-margin">
                            <select id="new-users" class="form-control" value="2" multiple="multiple" placeholder="Enter user names" name="conversation_users[]">

                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary row-margin create-conversation">Start Conversation</button>
                    </form>
                </div>
                <div class="conversation-thread" style="display:none;">
                    <div class="conversation-participants"></div>
                    <div class="conversation-messages"></div>
                    <textarea class="send-message form-control" ></textarea>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <script type="text/javascript">
        function formatUser (user) {
            if (!user.id) { return user.text; }
            var $user = $(
                    '<span><img class="card-img-top img-fluid pull-left" style="margin-right:5px;" src="'+ (user.avatar ? '{{ url('image/24x24/fit/').'/' }}' + user.avatar : 'http://www.gravatar.com/avatar/'+user.mailHash+'?s=24&d=retro')+'" /> ' + user.text + '</span>'
            );
            return $user;
        };
        $('#new-users').select2({
            ajax: {
                url: "{{ url('ajax/search') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatUser,
            allowClear: true
        });
        $(document).ready(function() {
            var hash = window.location.hash.substr(1);
            if (hash != 'new') {
                $('#conversation-'+hash).click();
            }
        })
    </script>
@endsection