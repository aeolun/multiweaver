<div class="participants text-right">
    @foreach($conversation->users as $user)
        <a href="{{ url('users/'.$user->id) }}" target="_blank" class="user-image">
            @if(strpos($user->email,'@') === false)
                <div class="user-no-mail" data-placement="bottom" data-toggle="tooltip" title="This person has no valid email, and no missed message notifications can be sent."><i class="fa fa-at text-danger"></i></div>
            @endif
            <img data-toggle="tooltip" title="{{ $user->name }}" style="background:#ccc;" height="40" width="40" src="{{ $user->avatar ? url('image/40x40/fit/'.$user->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($user->email))).'?s=40&d=retro' }}" alt="{{ $user->name }}">
            <div class="user-time">{{ (new \Carbon\Carbon())->setTimezone($user->timezone)->format('H:i') }}</div>
        </a>
    @endforeach
</div>
<div class="messages">
<div class="container-fluid">
    <div><div>
    <?php $currentDate = null;$currentUser = null; ?>
@foreach($messages as $idx=>$message)
    @if($currentDate != $message->created_at->setTimezone($timezone)->format('Y-m-d H') or $currentUser != $message->user->id)
</div>
</div>
        <div class="row message bg-glass" data-message-id="{{ $message->id }}">
            <div class="col-md-12">
                <img class="pull-left chat-image img-fluid" style="" src="{{ $message->user->avatar ? url('image/60x60/fit/'.$message->user->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($message->user->email))).'?s=60&d=retro' }}" alt="{{ $message->user->name }}">
            <div class="pull-right">
                <small><i>{{ $message->created_at->setTimezone($timezone)->format('Y-m-d H:i') }}</i></small>
            </div>
                {{! $currentDate = $message->created_at->setTimezone($timezone)->format('Y-m-d H') }}
                {{! $currentUser = $message->user->id }}
    @endif

            <div>{!! nl2br($message->text) !!}</div>
                @if(count($message->seenUsers) > 0)
                    @foreach($message->seenUsers as $user)
                        @if($user->id != Auth::user()->id)
                            <img data-toggle="tooltip" class="img-circle" title="Seen by {{ $user->name }}" style="background:#ccc;" height="20" width="20" src="{{ $user->avatar ? url('image/20x20/fit/'.$user->avatar) : 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($user->email))).'?s=20&d=retro' }}" alt="{{ $user->name }}">
                        @endif
                    @endforeach
                @endif
                <small><i>
                    @if(count($message->seenUsers) == 1 and $message->seenUsers[0]->id == Auth::user()->id)
                        Not seen yet
                    @endif
                    </i></small>

@endforeach
            </div>
        </div>
@if($conversation->messages()->count() == 0)
    <div style="padding:30px;">
        <p>There are currently no messages, type one and send it by pressing [enter] to start this conversation!</p>
        <p>You can make newlines by using [shift]+[enter].</p>
    </div>
@endif
</div>