/**
 * Created by aeolu on 15/02/2016.
 */
$(document).ready(function() {
	$(".available-time").removeClass('hidden-xl-down');
	$("#available-time").slider({
		formatter: function formatter(val) {
			if (Array.isArray(val)) {
				var from = val[0]+":00";
				var to = val[1]+":00";
				if (to == "24:00") to = "23:59";
				return from + " - " + to;
			} else {
				return val;
			}
		},
	});
	$('body').tooltip({
		selector: '[data-toggle="tooltip"]'
	});
	$('.popoverI').popover({
		html: true,
		placement: 'bottom',
		content: function() {
			var content = $(this).next().html();
			console.log(content);
			return content;
		}
	});
	$('.alert-status').delay(2000).fadeOut();
	$(document).on("click", '.pop', function(e) {
		$('#imagepreview').attr('src', $(this).attr('href')); // here asign the image to the modal when the user click the enlarge link
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
		e.stopPropagation();
		e.preventDefault();
		return false;
	});
})