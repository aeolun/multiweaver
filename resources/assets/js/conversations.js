/**
 * Created by aeolu on 18/01/2016.
 */
openConversationId = null;
function openConversation(id) {
	$('.conversation-thread').show();
	$('.new-conversation').hide();
	loadNewMessages(id);
	if (id != openConversationId) {
		$('.conversation-participants').load('conversations/'+id+' .participants');
	}
	openConversationId = id;
}
function loadNewMessages(id) {
	var doScroll = $('.conversation-messages').scrollTop() == $('.conversation-messages').get(0).scrollHeight - $('.conversation-messages').height();
	$('.conversation-messages').load('conversations/'+id+' .messages', function() {
		if (doScroll) {
			$('.conversation-messages').scrollTop($('.conversation-messages').get(0).scrollHeight);
		}
	});
}
function sendMessage() {
	$.ajax({
		url: 'conversations/'+openConversationId+'/message',
		method: 'POST',
		data: {
			conversation_id: openConversationId,
			text: $('.send-message').val()
		},
		success: function() {
			loadNewMessages(openConversationId);
		}
	});
	$('.send-message').val('');
}

$(document).ready(function() {
	$('.new-conversation-link').click(function() {
		$('.nav-pills a').removeClass('active');
		$(this).addClass('active');
		$('.conversation-thread').hide();
		$('.new-conversation').show();
		openConversationId = null;
	});
	$('.conversation-link').click(function() {
		$('.nav-pills a').removeClass('active');
		$(this).addClass('active');
		openConversation($(this).data('conversation-id'))
	});
	$('.create-conversation').click(function() {
		$.ajax({
			href: 'conversations',
			method: 'POST',
			data: $('.create-conversation-form').serialize(),
			success: function(data) {
				window.location.reload();
			}
		});
		return false;
	});
	$('.send-message').keydown(function(e) {
		if (e.which == 13 && e.shiftKey == false) {
			sendMessage();
			e.stopPropagation();
			e.preventDefault();
			return false;
		}
	})
	setInterval(function() {
		if (openConversationId != null) {
			loadNewMessages(openConversationId);
		}
	}, 5000);

});
